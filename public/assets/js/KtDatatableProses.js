var KTProses = {
	init:function(urlTarget,options,token) {
		$.extend(options);
		var dT = $('#kt_datatable').KTDatatable()
		$("body").on("click", ".btn-proses-on-table", function(e) {
			e.preventDefault();
			var a = $(this).attr("href");
			var judul = ($(this).data('judul') == undefined ? 'Apakah Anda Yakin?' : $(this).data('judul'));
			var textmsg = ($(this).data('textmsg') == undefined ? 'Data ini akan ter peroses secara otomatis' : $(this).data('textmsg'));
			var textok = ($(this).data('textok') == undefined ? 'Ok' : $(this).data('textok'));
			var textno = ($(this).data('textno') == undefined ? 'Batal' : $(this).data('textno'));
			swal.fire({
				title: judul,
				text: textmsg,
				type: "warning",
				showCancelButton: !0,
				confirmButtonText: textok,
				cancelButtonText: textno
			}).then(e => {
				e.value ? $.ajax({
					url: a,
					type: "get",
					dataType: "json",
					beforeSend: function() {
						swal.fire("Harap Menunggu", "Sedang memproses data.", "info")
					},
					success: function(e) {
						console.log(e.code)
						200 == e.code ? (swal.fire("Success", e.message, "success"), dT.reload()) : (e.code, swal.fire("Ooopp!!!", e.message, "error"))
					},
					error: function() {
						swal("Ooopp!!!", "Gagal memproses data, silahkan coba lagi", "error")
					}
				}) : e.dismiss
			})
		})
	}
};
jQuery(document).ready(function() {
	KTProses.init();
});