$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function showLoading() {
    $('#loadingpage').modal("show");
}

function hideLoading() {
    $('#loadingpage').modal("hide");
}