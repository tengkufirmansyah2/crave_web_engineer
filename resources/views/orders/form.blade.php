<table class="table table-bordered">
    <tr>
        <th width="150px">Gross Amount</th>
        <th width="1px">:</th>
        <td>{{number_format($data->gross_amount,0)}}</td>
    </tr>
    <tr>
        <th>Bank</th>
        <th>:</th>
        <td>{{$data->bank}}</td>
    </tr>
    <tr>
        <th>Va Number</th>
        <th>:</th>
        <td>{{$data->va_number}}</td>
    </tr>
    <tr>
        <th>Status</th>
        <th>:</th>
        <td class="form-group">
            <select name="status" class="form-control" required>
                <option value="Waiting" {{$data->payment_status == 'Waiting' ? 'selected' : ''}}>Waiting</option>
                <option value="Success" {{$data->payment_status == 'Success' ? 'selected' : ''}}>Success</option>
                <option value="Expired" {{$data->payment_status == 'Expired' ? 'selected' : ''}}>Expired</option>
                <option value="Cancelled" {{$data->payment_status == 'Cancelled' ? 'selected' : ''}}>Cancelled</option>
            </select>
        </td>
    </tr>
</table>
<h4>Invoice :</h4>
<table class="table table-bordered">
    <tr>
        <th>Customer Name</th>
        <th>Customer Email</th>
        <th>Subscription Type</th>
        <th>Total</th>
    </tr>
    <tr>
        <td>{{$data->invoice ? ($data->invoice->customer ? $data->invoice->customer->first_name.' '.$data->invoice->customer->last_name : '') : '' }}</td>
        <td>{{$data->invoice ? ($data->invoice->customer ? $data->invoice->customer->email : '') : '' }}</td>
        <td>{{$data->invoice ? ($data->invoice->subscription ? $data->invoice->subscription->name : '') : '' }}</td>
        <td>{{$data->invoice ? $data->invoice->total : '' }}</td>
    </tr>
</table>