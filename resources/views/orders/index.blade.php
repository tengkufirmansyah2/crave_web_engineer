@extends('components.template')
@section('title',__($title))
@section('content')
<div class="container-fluid p-0">
    <div class="row">
        <div class="col-12 col-lg-12">
                <div class="card card-custom gutter-b example example-compact">
                {{kt_datatable('Data '.$title, Request::url(), false)}}
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
var KTDatatableJsonRemoteDemo = function() {
    var dataTable = function() {
        var datatable = $('#kt_datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read : {
                        method: 'GET',
                        url: '{{ route('app.orders.index') }}',
                    }
                },
                pageSize: 10,
            },
            layout: {
                scroll: false,
                footer: false
            },

            sortable: true,
            pagination: true,
            search: {
                input: $('#kt_datatable_search_query'),
                key: 'generalSearch'
            },
            columns: [
            {
                field: 'Actions',
                title: 'Actions',
                sortable: false,
                autoHide: false,
                width : 70,
                overflow: 'visible',
                template: function(data, type, full, meta) {
                        console.log(data);
                        return '\
                        <div class="d-flex align-items-center">\
                        </div>\
                        <div class="dropdown dropdown-inline">\
                            <a href="javascript:;" class="btn btn-sm btn-clean btn-icon mr-2" data-toggle="dropdown">\
                                <span class="svg-icon svg-icon-md">\
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                            <rect x="0" y="0" width="24" height="24"/>\
                                            <path d="M5,8.6862915 L5,5 L8.6862915,5 L11.5857864,2.10050506 L14.4852814,5 L19,5 L19,9.51471863 L21.4852814,12 L19,14.4852814 L19,19 L14.4852814,19 L11.5857864,21.8994949 L8.6862915,19 L5,19 L5,15.3137085 L1.6862915,12 L5,8.6862915 Z M12,15 C13.6568542,15 15,13.6568542 15,12 C15,10.3431458 13.6568542,9 12,9 C10.3431458,9 9,10.3431458 9,12 C9,13.6568542 10.3431458,15 12,15 Z" fill="#000000"/>\
                                        </g>\
                                    </svg>\
                                </span>\
                            </a>\
                            <div class="dropdown-menu dropdown-menu-md dropdown-menu-left">\
                                <ul class="navi flex-column navi-hover py-2">\
                                    <li class="navi-header font-weight-bolder text-uppercase font-size-xs text-primary pb-2">\
                                        Choose an action:\
                                    </li>\
                                    <li class="navi-item">\
                                        <a href="{{url(Request::url()."/")}}'+data.id+'/edit" class="navi-link">\
                                            <span class="navi-icon"><i class="la la-edit"></i></span>\
                                            <span class="navi-text">Edit</span>\
                                        </a>\
                                    </li>\
                                </ul>\
                            </div>\
                        </div>\
                    ';
                },
            },
            {
                field: 'customer_id',
                title: 'Customer',
                template: function(row) {
                    return row.invoice ? (row.invoice.customer ? row.invoice.customer.first_name + ' ' + row.invoice.customer.last_name : '-' ) : '-';
                },
            },
            {
                field: 'subscription_id',
                title: 'Subscription',
                template: function(row) {
                    return row.invoice ? (row.invoice.subscription ? row.invoice.subscription.name : '-' ) : '-';
                },
            },
            {
                field: 'bank',
                title: 'Bank',
                template: function(row) {
                    return row.bank + '<br> VA : ' + row.va_number;
                },
            },
            {
                field: 'gross_amount',
                title: 'Gross Amount',
                template: function(row) {
                    return row.gross_amount ? Utils.numberLabelFormat(row.gross_amount) : '-';
                },
            },
            {
                field: 'payment_status',
                title: 'Payment Status',
            },
            {
                field: 'created_at',
                title: 'Created',
                template: function(row) {
                    return row.created_by_user ? row.created_by_user.name + '</br>' + Utils.dateIndonesia(row.created_at,true,true) : '-';
                },
            },
            {
                field: 'updated_at',
                title: 'Updated',
                template: function(row) {
                    return row.updated_by_user ? row.updated_by_user.name + '</br>' + Utils.dateIndonesia(row.updated_at,true,true) : '-';
                },
            }
            ],
        });
    };

    return {
        init: function() {
            dataTable();
        }
    };
}();

jQuery(document).ready(function() {
    KTDatatableJsonRemoteDemo.init();
});

</script>
<script src="{{ URL::asset('assets/js/KtDatatableDelete.js') }}"></script>
<script src="{{ URL::asset('assets/js/KtDatatableProses.js') }}"></script>
@endsection