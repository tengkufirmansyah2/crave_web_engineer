@extends('components.template')
@section('title','Beranda')
@section('title',__($title))
@section('content')

<div class="container-fluid p-0">
    <div class="mb-3">
        <h1 class="h3 d-inline align-middle">{{$title}}</h1>
    </div>
    <div class="row">
        <div class="col-12 col-lg-12">
            <div class="card">
                @if(Session::has('message'))
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <div class="alert-message">
                        {{ Session::get('message') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                </div>
                @endif
                <form method="POST" class="form-horizontal" enctype="multipart/form-data">
                    <div class="card-body">
                        {{ csrf_field() }}
                        @foreach($settings as $key => $val)
                        @if($val->type == 'image')
                        <div class="form-group">
                            <label class="col-form-label">{{$val->keyField}} <span class="text-danger">*</span></label>
                            <div class="col-lg-9 col-xl-6">
                                <div class="image-input image-input-outline" id="kt_image_{{$key}}">
                                    <div class="image-input-wrapper" style="background-image: url({{ URL::asset('/').$val->valueField }})"></div>
                                    <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                        <i class="fa fa-pen icon-sm text-muted"></i>
                                        <input type="file" name="{{$val->keyField}}" accept=".png, .jpg, .jpeg"/>
                                        <input type="hidden" name="{{$val->keyField}}_remove"/>
                                    </label>

                                    <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                        <i class="ki ki-bold-close icon-xs text-muted"></i>
                                    </span>
                                </div>
                                <span class="form-text text-muted">Allowed file types:  png, jpg, jpeg.</span>
                            </div>
                        </div>
                        @elseif($val->type == 'textarea')
                        <div class="mb-3">
                            <label class="form-label">{{$val->keyField}} <span class="text-danger">*</span></label>
                            <textarea name="{{$val->keyField}}" required="true" class="form-control">{{$val->valueField}}</textarea>
                        </div>
                        @else
                        <div class="mb-3">
                            <label class="form-label">{{$val->keyField}} <span class="text-danger">*</span></label>
                            <input type="{{$val->type}}" class="form-control" name="{{$val->keyField}}" value="{{$val->valueField}}" placeholder="Table Name" required="true" />
                        </div>
                        @endif
                        @endforeach
                    </div>
                    <div class="card-footer">
                        <input class="btn btn-primary" type="submit" value="Simpan">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@stop
@section('script')
@foreach($settings as $key => $val)
@if($val->type == 'image')
<script type="text/javascript">
    var avatar{{$key}} = new KTImageInput('kt_image_{{$key}}');
</script>
@endif
@endforeach
@stop