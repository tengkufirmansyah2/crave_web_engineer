@extends('components.template')
@section('title',__($title))
@section('content')

<div class="main d-flex flex-column flex-row-fluid">
	<!--begin::Entry-->
	<div class="content flex-column-fluid" id="kt_content">
		<div class="row">
			<div class="col-md-12">
				<!--begin::Notice-->
				@if(Session::has('message'))
				<div class="alert alert-custom alert-white alert-shadow gutter-b fade show" role="alert">
					<div class="alert-icon">
						<i class="far fa-bell"></i>
					</div>
					<div class="alert-text">
						{{ Session::get('message') }}
					</div>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				@endif
				<!--end::Notice-->

				<!--begin::Card-->
				<div class="card card-custom">
					<div class="card-header">
						<h3 class="card-title">
							{{$title}}
						</h3>
                        <div class="card-toolbar">
                            <div class="example-tools justify-content-center">
                                <a href="{{ route('app.role.index') }}" class="btn btn-info"><i class="ki ki-long-arrow-back"></i> Kembali</a>
                            </div>
                        </div>
					</div>
					<!--begin::Form-->
					<form method="POST" class="form-horizontal" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="card-body">
							<table class="table table-bordered potongan">
								<thead>
									<tr>
										<th>Nama</th>
										<th>Position</th>
										<th><input type="checkbox" id="select-all" /> Akses</th>
									</tr>
								</thead>
								<tbody>
									@foreach($config as $key => $val)
									<tr>
										<td>{{$val->parent ? $val->parent->name.' -> ' : ''}}{{$val->name}}</td>
										<td>{{$val->position}}</td>
										<td>
											<input type="checkbox" name="akses[]" value="{{$val->id}}" {{ ($val->role_id != null) ? 'checked' : ''}}>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						<div class="card-footer">
							<input class="btn btn-primary" type="submit" value="Save">
						</div>
					</form>
					<!--end::Form-->
				</div>
			</div>
		</div>
	</div>
</div>
@stop
@section('script')
<script type="text/javascript">
	$('#select-all').click(function(event) {   
		if(this.checked) {
        // Iterate each checkbox
        $(':checkbox').each(function() {
        	this.checked = true;                        
        });
    } else {
    	$(':checkbox').each(function() {
    		this.checked = false;                       
    	});
    }
});
</script>
@stop