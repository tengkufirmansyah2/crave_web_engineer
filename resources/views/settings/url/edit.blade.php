@extends('components.template')
@section('title',$title)
@section('content')
<div class="main d-flex flex-column flex-row-fluid">
    @include('components.breadcrumb')
    <div class="content flex-column-fluid" id="kt_content">
        <div class="row">
            <div class="col-md-12">
                <!--begin::Card-->
                <div class="card card-custom gutter-b example example-compact">
                    <div class="card-header">
                        <h3 class="card-title">{{$title}}</h3>
                        <div class="card-toolbar">
                            <div class="example-tools justify-content-center">
                                <a href="{{ route('app.url.index') }}" class="btn btn-info"><i class="ki ki-long-arrow-back"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
                    <form id="form">
                        <input type="hidden" name="_method" value="{{ $edit == true ? 'PATCH' : 'POST' }}">
                        {{ method_field('PATCH') }}
                        @csrf
                        <div class="card-body">
                            <div class="validation-message"></div>
                            @include ('components.caution', ['title' => $title])
                            @include ('settings.url.form', ['formMode' => 'create'])
                        </div>
                        <div class="card-footer">
                            <button type="submit" id="submit" class="btn btn-primary mr-2">{{ $edit == true ? 'Ubah url' : 'Tambah Baru' }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        const fv = FormValidation.formValidation(
			document.getElementById('form'),
			{
				fields: {
					parent_id: {
						validators: {
							stringLength: {
								max:36,
								message: 'Maximum is 36 characters.'
							}
						}
					},
					nama: {
						validators: {
							notEmpty: {
								message: 'Nama Url!'
							},
							stringLength: {
								max:25,
								message: 'Maximum is 25 characters.'
							}
						}
					},
                    url: {
                        validators: {
                            notEmpty: {
                                message: 'Url!'
                            },
                            stringLength: {
                                max:25,
                                message: 'Maximum is 25 characters.'
                            }
                        }
                    },
                    url: {
                        validators: {
                            stringLength: {
                                max:500,
                                message: 'Maximum is 500 characters.'
                            }
                        }
                    },
					order: {
						validators: {
							intLength: {
								max:11,
								message: 'Maximum is 11 characters.'
							}
						}
					},
					position: {
						validators: {
                            notEmpty: {
                                message: 'Position!'
                            },
							stringLength: {
								max:25,
								message: 'Maximum is 25 characters.'
							}
						}
					}
                },
                plugins:{
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    SubmitButton: new FormValidation.plugins.SubmitButton()
                }
            }
        ).on('core.form.valid', function() {
            var formData = $("#form").serialize();
            showLoading();
            $.post('{{ $edit == true ? route('app.url.update',[app('request')->url])  : route('app.url.store') }}', formData)
            .done(function(data){
                Swal.fire("Berhasil!", "{{ $edit == true ? 'Successfully changed data url!' : 'New Url data added successfully!' }}", "success");
                hideLoading();
                window.location.href = "{{ route('app.url.index') }}";
            }).fail(function(xhr,status,error){
                var errs = '<ul style="display:block">'
                if(xhr.status == 422){
                    p = xhr.responseJSON.errors;
                    for(var key in p){
                        p[key].forEach(err => {
                            errs += '<li><label class="error">'+err+'</label></li>'
                        })
                    }
                    errs += '</ul>';
                }else{
                    errs += '<li><label class="error">'+xhr.responseJSON.message+'</label></li>';
                }
                $('.validation-message').html(errs)
                hideLoading()
            });
        });
    });
</script>
@endsection

