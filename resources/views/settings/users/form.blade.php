<div class="form-group">
    <label>Nama<span class="text-danger">*</span></label>
    <input type="text" class="form-control" name="name" placeholder="Nama Akun" value="{{ isset($data->name) ? $data->name : old('name') }}">
</div>
<div class="form-group">
    <label>Email<span class="text-danger">*</span></label>
    <input type="email" name="email" class="form-control" required placeholder="Ketik Email" value="{{ isset($data->email) ? $data->email : old('email') }}" />
</div>
<div class="form-group">
    <label>Password @if($edit == false)<span class="text-danger">*</span>@endif</label>
    <input type="password" class="form-control" name="password" placeholder="Ketik Password" {{ $edit == false ? 'required' : ''}} value="">
</div>
<div class="form-group">
    <label>Konfirmasi Password @if($edit == false)<span class="text-danger">*</span>@endif</label>
    <input type="password" class="form-control" name="password_confirm" placeholder="Ketik Ulang Password" {{ $edit == false ? 'required' : ''}} value="">
</div>
<div class="form-group">
    <label>Role<span class="text-danger">*</span></label>
    <select name="role_id" class="form-control" required>
        @if($edit == false)
        <option value="">-- Pilih Role --</option>
        @endif
        @foreach($role as $rl)
        <option value="{{ $rl->id }}" {{ $edit == true ? ($data->role_id == $rl->id ? 'selected' : '') : '' }} {{ old('role_id') == $rl->id ? 'selected' : '' }}>{{ ucfirst($rl->name) }}</option>
        @endforeach
    </select>
</div>
@if($edit == true)
@if($data->email_verified_at == null)
<div class="form-check">
    <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" name="email_verified">
    <label class="form-check-label" for="flexCheckDefault">
        Account Verification
    </label>
</div>
@endif
@endif