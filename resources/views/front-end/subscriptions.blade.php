@extends('components.front-end')
@section('title',__('Detail'))
@section('content')
<main class="container">
    @if(Session::has('message'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <div class="alert-message">
            {{ Session::get('message') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    </div>
    @endif
    <br>
    <h2>Status Subscription :</h2>
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Packet</th>
            <th>Start</th>
            <th>End</th>
            <th>Status</th>
        </tr>
        @foreach($list as $key => $ls)
        <tr>
            <td width="5px">{{$key+1}}</td>
            <td>
                {{isset($ls->subscription) ? $ls->subscription->name : '-'}}
            </td>
            <td>
                {{beautiDate($ls->start_date)}}
            </td>
            <td>
                {{beautiDate($ls->end_date)}}
            </td>
            <td>
                @if($ls->end_date < date('Y-m-d'))
                Expired
                @else
                Active
                @endif
            </td>
        </tr>
        @endforeach
    </table>
    <h2>Status Order :</h2>
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Va Number</th>
            <th>Bank</th>
            <th>Total</th>
            <th>Packet</th>
        </tr>
        @foreach($order as $key => $or)
        <tr>
            <td width="5px">{{$key+1}}</td>
            <td>{{$or->va_number}}</td>
            <td>{{$or->bank}}</td>
            <td>{{$or->gross_amount}}</td>
            <td>{{$or->name}}</td>
            <td>{{$or->payment_status}}</td>
        </tr>
        @endforeach
    </table>
    <div class="pricing-header p-3 pb-md-4 mx-auto text-center">
        <h1 class="display-4 fw-normal">Pricing Subscriptions</h1>
    </div>
    <div class="row row-cols-1 row-cols-md-3 mb-3 text-center">
        @foreach($subscriptions as $val)
        <div class="col">
            <div class="card mb-4 rounded-3 shadow-sm">
                <div class="card-header py-3">
                    <h4 class="my-0 fw-normal">{{$val->name}}</h4>
                </div>
                <div class="card-body">
                    <h2 class="card-title pricing-card-title">Rp. {{number_format($val->value,0)}}<small class="text-muted fw-light">/mo</small></h2>
                    <button type="button" class="w-100 btn btn-lg btn-outline-primary buy" data-id="{{$val->id}}">Get / Buy</button>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</main>
@endsection
@section('script')
<script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="{{ config('midtrans.client_key') }}"></script>
<script type="text/javascript">
    @if($pay == true)
    $( document ).ready(function() {
        snap.pay('{{ $snapToken }}', {
            // Optional
            onSuccess: function(result) {
                $.post("{{ route('app.invoice.payment.update',[$id]) }}",
                    result,
                    function(data,status){
                        Swal.fire("Berhasil!", "Update data Invoice!", status);
                        window.location.href = "{{ url('subscriptions') }}";
                    });
                console.log('success', result)
            },
            // Optional
            onPending: function(result) {
                $.post("{{ route('app.invoice.payment.update',[$id]) }}",
                    result,
                    function(data,status){
                        Swal.fire("Berhasil!", "Update data Invoice!", status);
                        window.location.href = "{{ url('subscriptions') }}";
                    });
                console.log('pending',result)
            },
            // Optional
            onError: function(result) {
                /* You may add your own js here, this is just example */
                // document.getElementById('result-json').innerHTML += JSON.stringify(result, null, 2);
                window.location.href = "{{ url('subscriptions') }}";
                console.log('error',result)
            }
        });
    });
    @endif
    $(".buy").click(function(){
        var y = $(this).data();
        showLoading();
        $.ajax({
            type: "GET",
            url : "{{url('/subscriptions/check').'/'}}"+y.id,
            success: function(addr){
                hideLoading();
                if (addr.status == 'success') {
                    Swal.fire("Success!", addr.message, "success");
                    window.location.href = "{{url('/subscriptions/payment').'/'}}"+y.id;
                }else{
                    Swal.fire("Failed!", addr.message, "error");
                }
            }
        });
    });
</script>
@endsection