@extends('components.front-end')
@section('title',__('Home'))
@section('content')
<main>
    <section class="py-5 text-center container">
        <div class="row py-lg-5">
            <div class="col-lg-6 col-md-8 mx-auto">
                <h1 class="fw-light">{{ app_setting()['App_Name']['valueField'] }}</h1>
                <p class="lead text-muted">{{ app_setting()['App_Description']['valueField'] }}</p>
            </div>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">

            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                @foreach($data as $val)
                <div class="col">
                    <div class="card shadow-sm">
                        <img src="{{ 'http://'.$val->path.'/thumbnail/'.$val->image }}" >
                        <label class="type-subscription badge bg-secondary">
                            {{isset($val->subscription) ? $val->subscription->name : '-'}}
                        </label>
                        <div class="card-body">
                            <h4>{{$val->name}}</h4>
                            <p class="card-text">{!!$val->slug!!}</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <a href="{{url('/template/'.$val->id)}}" type="button" class="btn btn-sm btn-outline-secondary">View</a>
                                    <a href="{{url('/template/proses').'/'. $val->id}}" type="button" class="btn btn-sm btn-outline-secondary">Download</a>
                                </div>
                                <small class="text-muted">{{beautiDate($val->created_at).' '.date('H:i:s', strtotime($val->created_at))}}</small>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            {!! $data->withQueryString()->links('pagination::bootstrap-5') !!}
        </div>
    </div>
</main>
@endsection