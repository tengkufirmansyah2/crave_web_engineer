<div class="form-group">
    <label>Group<span class="text-danger">*</span></label>
    <input type="text" class="form-control" name="group" placeholder="Parameter group" value="{{ isset($data->group) ? $data->group : old('group') }}">
</div>
<div class="form-group">
    <label>Name<span class="text-danger">*</span></label>
    <input type="text" class="form-control" name="name" placeholder="Parameter Name" value="{{ isset($data->name) ? $data->name : old('name') }}">
</div>
<div class="form-group">
    <label>Value<span class="text-danger">*</span></label>
    <input type="text" class="form-control" name="value" placeholder="Parameter value" value="{{ isset($data->value) ? $data->value : old('value') }}">
</div>
<div class="form-group row">
    <label class="col-xl-3 col-lg-3 col-form-label">Image</label>
    <div class="col-lg-9 col-xl-6">
        <div class="image-input image-input-outline" id="kt_image_1">
            <div class="image-input-wrapper" style="background-image: url(//{{ isset($data) ? $data->image : '' }})"></div>
            <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                <i class="fa fa-pen icon-sm text-muted"></i>
                <input type="file" name="image" accept=".png, .jpg, .jpeg"/>
                <input type="hidden" name="image_remove"/>
            </label>

            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                <i class="ki ki-bold-close icon-xs text-muted"></i>
            </span>
        </div>
        <span class="form-text text-muted">Allowed file types:  png, jpg, jpeg.</span>
    </div>
</div>  