@extends('components.template')
@section('title','Beranda')
@section('content')

<div class="container-fluid p-5">
    @if(permissionCheck(app('request'),'dashboard-administrator'))
    {{-- <h1 class="mb-5 mt-3">Admin Dashboard</h1> --}}
    <h1 class="mb-5">Admin Dashboard</h1>
    @include ('components.alert', ['title' => $title])
    <div class="row">
        <div class="col-12 col-sm-6 col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div class="float-end"><h1 class="stat-number">{{ number_format($customers, 0) }}</h1></div>
                    <div class="float-nonen">
                        <h3><i data-feather="users" class="mr-2"></i>Customers</h3>
                    </div>
                    <div class="float-none small"><br><br><span>Last registered new customer on . . .</span></div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div class="float-end"><h1 class="stat-number">{{ number_format($countries, 0) }}</h1></div>
                    <div class="float-none">
                        <h3><i data-feather="flag" class="mr-2"></i>Countries</h3>
                    </div>
                    <div class="float-none small"><br><br><span>Last registered new country on . . .</span></div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div class="float-end"><h1 class="stat-number">{{ number_format($states, 0) }}</h1></div>
                    <div class="float-none">
                        <h3><i data-feather="map" class="mr-2"></i>Provinces</h3>
                    </div>
                    <div class="float-none small"><br><br><span>Last registered new province on . . .</span></div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-6 col-md-6 col-xl-3">
            <div class="card">
                <div class="card-body">
                    <div class="float-end"><h1 class="stat-number">{{ number_format($cities, 0) }}</h1></div>
                    <div class="float-none">
                        <h3><i data-feather="map-pin" class="mr-2"></i>Cities</h3>
                    </div>
                    <div class="float-none small"><br><br><span>Last registered new cities on . . .</span></div>
                </div>
            </div>
        </div>
    </div>
    @else
    <h1 class="px-0">Customer Dashboard</h1>
    @include ('components.welcome', ['title' => $title])
    @endif
</div>
@endsection