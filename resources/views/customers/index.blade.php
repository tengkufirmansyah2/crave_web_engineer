@extends('components.template')
@section('title',__($title))
@section('content')
<div class="container-fluid p-0">
    <div class="row">
        <div class="col-12 col-lg-12">
                <div class="card card-custom gutter-b example example-compact">
                {{kt_datatable('Data '.$title, Request::url(), false)}}
            </div>
        </div>
    </div>
</div>

@endsection
@section('script')
<script>
var KTDatatableJsonRemoteDemo = function() {
    var dataTable = function() {
        var datatable = $('#kt_datatable').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read : {
                        method: 'GET',
                        url: '{{ route('app.customers.index') }}',
                    }
                },
                pageSize: 10,
            },
            layout: {
                scroll: false,
                footer: false
            },

            sortable: true,
            pagination: true,
            search: {
                input: $('#kt_datatable_search_query'),
                key: 'generalSearch'
            },
            columns: [
            {
                field: 'role',
                title: 'Role',
                template: function(row) {
                    return row.user ? row.user.role ? row.user.role.name : '-' : '-';
                },
            },
            {
                field: 'name',
                title: 'Name',
                template: function(row) {
                    return row.first_name ? row.first_name + ' ' + row.last_name : '-';
                },
            },
            {
                field: 'email',
                title: 'Email',
            },
            {
                field: 'mobile',
                title: 'Mobile',
            },
            {
                field: 'subscription',
                title: 'Subscription',
                template: function(row) {
                    return row.subscription ? row.subscription.name : '-';
                },
            },
            {
                field: 'country_id',
                title: 'Country',
                template: function(row) {
                    return row.country ? row.country.name : '-';
                },
            },
            {
                field: 'state_id',
                title: 'State',
                template: function(row) {
                    return row.state ? row.state.name : '-';
                },
            },
            {
                field: 'city_id',
                title: 'City',
                template: function(row) {
                    return row.city ? row.city.name : '-';
                },
            },
            {
                field: 'created_at',
                title: 'Created',
                template: function(row) {
                    return row.created_by_user ? row.created_by_user.name + '</br>' + Utils.dateIndonesia(row.created_at,true,true) : '-';
                },
            },
            {
                field: 'updated_at',
                title: 'Updated',
                template: function(row) {
                    return row.updated_by_user ? row.updated_by_user.name + '</br>' + Utils.dateIndonesia(row.updated_at,true,true) : '-';
                },
            }
            ],
        });
    };

    return {
        init: function() {
            dataTable();
        }
    };
}();

jQuery(document).ready(function() {
    KTDatatableJsonRemoteDemo.init();
});

</script>
<script src="{{ URL::asset('assets/js/KtDatatableDelete.js') }}"></script>
<script src="{{ URL::asset('assets/js/KtDatatableProses.js') }}"></script>
@endsection