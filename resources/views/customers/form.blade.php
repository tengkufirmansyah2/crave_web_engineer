<div class="form-group">
    <label>Group<span class="text-danger">*</span></label>
    <input type="text" class="form-control" name="group" placeholder="Parameter group" value="{{ isset($data->group) ? $data->group : old('group') }}">
</div>
<div class="form-group">
    <label>Name<span class="text-danger">*</span></label>
    <input type="text" class="form-control" name="name" placeholder="Parameter Name" value="{{ isset($data->name) ? $data->name : old('name') }}">
</div>
<div class="form-group">
    <label>Value<span class="text-danger">*</span></label>
    <input type="text" class="form-control" name="value" placeholder="Parameter value" value="{{ isset($data->value) ? $data->value : old('value') }}">
</div>