@extends('components.front-end')
@section('title',__($title))
@section('content')
<main class="container">
    <br>
    <div class="container-fluid p-0">
        <div class="row">
            @if(Session::has('message'))
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <div class="alert-message">
                    {{ Session::get('message') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
            @endif
            @include('profile.menus.index')
            <div class="col-md-8 col-xl-9">
                @include ('components.alert', ['title' => $title])
                <form method="POST" class="form-horizontal" enctype="multipart/form-data" id="form">
                    <input type="hidden" name="_method" value="POST">
                    @csrf
                    <div class="card card-custom">
                        <div class="card-header">
                            <div class="card-title align-items-start flex-column">
                                <h5 class="card-title mb-0">Profile</h5>
                            </div>
                        </div>
                        <div class="card-body h-100">
                            <div class="validation-message"></div>
                            @include ('profile.input.profile', ['formMode' => 'create'])
                        </div>
                        <div class="card-footer">
                            <button type="submit" id="submit" class="btn btn-success mr-2">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
@endsection
@section('script')
<script>
    $(document).ready(function(){
        const fv = FormValidation.formValidation( document.getElementById('form'),{
            fields: {
                first_name: {
                    validators: {
                        notEmpty: {
                            message: 'First Name!'
                        },
                        stringLength: {
                            max:50,
                            message: 'Maximum is 50 characters.'
                        }
                    }
                },
                last_name: {
                    validators: {
                        notEmpty: {
                            message: 'Last Name!'
                        },
                        stringLength: {
                            max:50,
                            message: 'Maximum is 50 characters.'
                        }
                    }
                },
                birth_place: {
                    validators: {
                        notEmpty: {
                            message: 'Birth Place!'
                        },
                        stringLength: {
                            max:50,
                            message: 'Maximum is 50 characters.'
                        }
                    }
                },
                birth_date: {
                    validators: {
                        notEmpty: {
                            message: 'Birth Date!'
                        }
                    }
                },
                gender_id: {
                    validators: {
                        notEmpty: {
                            message: 'Gender!'
                        }
                    }
                },
                nationality_id: {
                    validators: {
                        notEmpty: {
                            message: 'Nationality!'
                        }
                    }
                },
                religion_id: {
                    validators: {
                        notEmpty: {
                            message: 'Religion!'
                        }
                    }
                },
                marital_id: {
                    validators: {
                        notEmpty: {
                            message: 'Marital!'
                        }
                    }
                },
                email: {
                    validators: {
                        emailAddress:{
                            message: 'Invalid email!'
                        },
                        notEmpty: {
                            message: 'Email is required!'
                        },
                        stringLength: {
                            max:100,
                            message: 'Maximum is 100 characters.'
                        }
                    }
                },
                mobile: {
                    validators: {
                        notEmpty: {
                            message: 'Mobile!'
                        },
                        stringLength: {
                            max:50,
                            message: 'Maximum is 50 characters.'
                        }
                    }
                },
                address: {
                    validators: {
                        notEmpty: {
                            message: 'Address!'
                        },
                        stringLength: {
                            max:255,
                            message: 'Maximum is 255 characters.'
                        }
                    }
                },
            },
            plugins:{
                trigger: new FormValidation.plugins.Trigger(),
                // Bootstrap Framework Integration
                bootstrap: new FormValidation.plugins.Bootstrap(),
                // Validate fields when clicking the Submit button
                SubmitButton: new FormValidation.plugins.SubmitButton()
            }
        }
        ).on('core.form.valid', function() {
            var formData = $("#form").serialize();
            showLoading();
            $.ajax({
                url: "{{ route('profile.update') }}",
                type: "POST",
                dataType: "JSON",
                data: new FormData($('#form')[0]),
                processData: false,
                contentType: false,
                success: function (data)
                {
                    Swal.fire("Berhasil!", "Successfully changed Profile data!", "success");
                    hideLoading();
                    window.location.href = "{{ route('profile.index') }}";
                },
                error: function (xhr, status, error)
                {
                    var errs = '<ul style="display:block">'
                    if(xhr.status == 422){
                        p = xhr.responseJSON.errors;
                        for(var key in p){
                            p[key].forEach(err => {
                                errs += '<li><label class="error">'+err+'</label></li>'
                            })
                        }
                        errs += '</ul>';
                    }else{
                        errs += '<li><label class="error">'+xhr.responseJSON.message+'</label></li>';
                    }
                    $('.validation-message').html(errs)
                    hideLoading()
                }
            });
        });
    });

$("#gender_id").focus(function(){
    var val = 'GENDER', i;
    var y = $(this)[0];
    $.ajax({
        type: "GET",
        url : "{{url('profile/get-params').'/'}}"+val,
        success: function(addr){
            y.innerHTML = "<option> </option>"
            for(i = 0; i < addr.Data.length; i++){
                var option = document.createElement("option");
                option.value = addr.Data[i].id;
                option.text = addr.Data[i].name;
                y.add(option);
            }
            return false;
        }
    });
});

$("#nationality_id").focus(function(){
    var val = 'NATIONALITY', i;
    var y = document.getElementById('nationality_id');
    $.ajax({
        type: "GET",
        url : "{{url('profile/get-params').'/'}}"+val,
        success: function(addr){
            y.innerHTML = "<option> </option>"
            for(i = 0; i < addr.Data.length; i++){
                var option = document.createElement("option");
                option.value = addr.Data[i].id;
                option.text = addr.Data[i].name;
                y.add(option);
            }
            return false;
        }
    });
});

$("#religion_id").focus(function(){
    var val = 'RELIGION', i;
    var y = document.getElementById('religion_id');
    $.ajax({
        type: "GET",
        url : "{{url('profile/get-params').'/'}}"+val,
        success: function(addr){
            y.innerHTML = "<option> </option>"
            for(i = 0; i < addr.Data.length; i++){
                var option = document.createElement("option");
                option.value = addr.Data[i].id;
                option.text = addr.Data[i].name;
                y.add(option);
            }
            return false;
        }
    });
});

$("#marital_id").focus(function(){
    var val = 'MARITAL', i;
    var y = document.getElementById('marital_id');
    $.ajax({
        type: "GET",
        url : "{{url('profile/get-params').'/'}}"+val,
        success: function(addr){
            y.innerHTML = "<option> </option>"
            for(i = 0; i < addr.Data.length; i++){
                var option = document.createElement("option");
                option.value = addr.Data[i].id;
                option.text = addr.Data[i].name;
                y.add(option);
            }
            return false;
        }
    });
});

$("#country_id").change(function(){
    var val = $(this).val(), i;
    var y = document.getElementById('state_id');
    y.innerHTML = "<option>Please Wait... </option>";
    $.ajax({
        type: "GET",
        url : "{{url('app/get-states').'/'}}"+val,
        success: function(addr){
            y.innerHTML = "<option> </option>"
            for(i = 0; i < addr.Data.length; i++){
                var option = document.createElement("option");
                option.value = addr.Data[i].id;
                option.text = addr.Data[i].name;
                y.add(option);
            }
            return false;
        }
    });
});

$("#state_id").change(function(){
    var val = $(this).val(), i;
    var y = document.getElementById('city_id');
    y.innerHTML = "<option>Please Wait... </option>";
    $.ajax({
        type: "GET",
        url : "{{url('app/get-cities').'/'}}"+val,
        success: function(addr){
            y.innerHTML = "<option> </option>"
            for(i = 0; i < addr.Data.length; i++){
                var option = document.createElement("option");
                option.value = addr.Data[i].id;
                option.text = addr.Data[i].name;
                y.add(option);
            }
            return false;
        }
    });
});

var avatar1 = new KTImageInput('kt_image_1');
</script>
@stop