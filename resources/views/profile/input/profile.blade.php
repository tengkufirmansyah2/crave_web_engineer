<div class="form-group row mb-3">
	<label class="col-xl-3 col-lg-3 col-form-label">First Name<span class="text-danger">*</span></label>
	<div class="col-lg-9 col-xl-6">
		<input class="form-control form-control-lg" type="text" name="first_name" value="{{ isset($data) ? $data->first_name : old('first_name') }}" placeholder="First Name" />
	</div>
</div>
<div class="form-group row mb-3">
	<label class="col-xl-3 col-lg-3 col-form-label">Last Name<span class="text-danger">*</span></label>
	<div class="col-lg-9 col-xl-6">
		<input class="form-control form-control-lg" type="text" name="last_name" value="{{ isset($data) ? $data->last_name : old('last_name') }}" placeholder="Last Name" />
	</div>
</div>
<div class="form-group row mb-3">
	<label class="col-xl-3 col-lg-3 col-form-label">Birth Place<span class="text-danger">*</span></label>
	<div class="col-lg-9 col-xl-6">
		<input class="form-control form-control-lg" type="text" name="birth_place" value="{{ isset($data) ? $data->birth_place : old('birth_place') }}" placeholder="Birth Place" />
	</div>
</div>
<div class="form-group row mb-3">
	<label class="col-xl-3 col-lg-3 col-form-label">Birth Date<span class="text-danger">*</span></label>
	<div class="col-lg-9 col-xl-6">
		<input class="form-control form-control-lg" type="date" name="birth_date" value="{{ isset($data) ? $data->birth_date : old('birth_date') }}" placeholder="Birth Date" />
	</div>
</div>
<div class="form-group row mb-3">
	<label class="col-xl-3 col-lg-3 col-form-label">Gender<span class="text-danger">*</span></label>
	<div class="col-lg-9 col-xl-6">
		<select name="gender_id" id="gender_id" class="form-control" required>
			@if(isset($data))
			<option value="{{ $data->gender_id }}">{{ ucfirst($data->gender ? $data->gender->name : '') }}</option>
			@endif
		</select>
	</div>
</div>
<div class="form-group row mb-3">
	<label class="col-xl-3 col-lg-3 col-form-label">Religion<span class="text-danger">*</span></label>
	<div class="col-lg-9 col-xl-6">
		<select name="religion_id" id="religion_id" class="form-control" required>
			@if(isset($data))
			<option value="{{ $data->religion_id }}">{{ ucfirst($data->religion ? $data->religion->name : '') }}</option>
			@endif
		</select>
	</div>
</div>
<div class="form-group row mb-3">
	<label class="col-xl-3 col-lg-3 col-form-label">Email<span class="text-danger">*</span></label>
	<div class="col-lg-9 col-xl-6">
		<input class="form-control form-control-lg" type="email" name="email" value="{{ isset($data) ? $data->email : old('email') }}" placeholder="Your Email" />
	</div>
</div>
<div class="form-group row mb-3">
	<label class="col-xl-3 col-lg-3 col-form-label">Mobile<span class="text-danger">*</span></label>
	<div class="col-lg-9 col-xl-6">
		<input class="form-control form-control-lg" type="text" name="mobile" value="{{ isset($data) ? $data->mobile : old('mobile') }}" placeholder="(021) ***" />
	</div>
</div>
<div class="form-group row mb-3">
	<label class="col-xl-3 col-lg-3 col-form-label">Blood Type</label>
	<div class="col-lg-9 col-xl-6">
		<input class="form-control form-control-lg" type="text" name="blood_type" value="{{ isset($data) ? $data->blood_type : old('blood_type') }}" placeholder="Blood Type" />
	</div>
</div>
<div class="form-group row mb-3">
	<label class="col-xl-3 col-lg-3 col-form-label">Address<span class="text-danger">*</span></label>
	<div class="col-lg-9 col-xl-6">
		<textarea class="form-control" name="address">{{ isset($data) ? $data->address : old('address') }}</textarea>
	</div>
</div>
<div class="form-group row mb-3">
	<label class="col-xl-3 col-lg-3 col-form-label">Country</label>
	<div class="col-lg-9 col-xl-6">
		<select name="country_id" id="country_id" class="form-control mb-3">
			<option value="">-- Select Country --</option>
			@foreach($countries as $ct)
			<option value="{{ $ct->id }}" {{ isset($data) ? ($data->country_id == $ct->id ? 'selected' : '') : '' }} {{ old('country_id') == $ct->id ? 'selected' : '' }}>{{ ucfirst($ct->name) }}</option>
			@endforeach
		</select>
	</div>
</div>
<div class="form-group row mb-3">
	<label class="col-xl-3 col-lg-3 col-form-label">State</label>
	<div class="col-lg-9 col-xl-6">
		<select name="state_id" id="state_id" class="form-control">
			@if(isset($data))
			<option value="{{ $data->state_id }}">{{ ucfirst($data->state ? $data->state->name : '') }}</option>
			@endif
		</select>
	</div>
</div>
<div class="form-group row mb-3">
	<label class="col-xl-3 col-lg-3 col-form-label">City</label>
	<div class="col-lg-9 col-xl-6">
		<select name="city_id" id="city_id" class="form-control">
			@if(isset($data))
			<option value="{{ $data->city_id }}">{{ ucfirst($data->city ? $data->city->name : '') }}</option>
			@endif
		</select>
	</div>
</div>
<div class="form-group row mb-3">
	<label class="col-xl-3 col-lg-3 col-form-label">Zip Code</label>
	<div class="col-lg-9 col-xl-6">
		<input class="form-control form-control-lg" type="text" name="zip_code" value="{{ isset($data) ? $data->zip_code : old('zip_code') }}" placeholder="Zip Code" />
	</div>
</div>
<div class="form-group row mb-3">
	<label class="col-xl-3 col-lg-3 col-form-label">Photo Profile</label>
	<div class="col-lg-9 col-xl-6">
		<div class="image-input image-input-outline" id="kt_image_1">
			<div class="image-input-wrapper" style="background-image: url(//{{ isset($data) ? $data->photo_path.'/'.$data->photo_name : '' }})"></div>
			<label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
				<i class="icon-sm text-muted">
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pen-fill" viewBox="0 0 16 16">
					  	<path d="m13.498.795.149-.149a1.207 1.207 0 1 1 1.707 1.708l-.149.148a1.5 1.5 0 0 1-.059 2.059L4.854 14.854a.5.5 0 0 1-.233.131l-4 1a.5.5 0 0 1-.606-.606l1-4a.5.5 0 0 1 .131-.232l9.642-9.642a.5.5 0 0 0-.642.056L6.854 4.854a.5.5 0 1 1-.708-.708L9.44.854A1.5 1.5 0 0 1 11.5.796a1.5 1.5 0 0 1 1.998-.001z"/>
					</svg>
				</i>
				<input type="file" name="photo_name" accept=".png, .jpg, .jpeg"/>
				<input type="hidden" name="photo_name_remove"/>
			</label>

			<span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
				<i class="icon-xs text-muted">
					<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16">
					  	<path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
					</svg>
				</i>
			</span>
		</div>
		<span class="form-text text-muted">Allowed file types:  png, jpg, jpeg.</span>
	</div>
</div>