<div class="col-md-4 col-xl-3">
	<div class="card mb-3">
		<div class="card-header">
			<h5 class="card-title mb-0">My Profile</h5>
		</div>
		<div class="card-body text-center">
			<img src="{{ fotoProfilThumbnail() }}"  alt="Avatar Admin" class="img-fluid rounded-circle mb-2" width="128" height="128" />
			<h5 class="card-title mb-0">{{Auth::user()->name}}</h5>
			<div class="text-muted mb-2">{{Auth::user()->email}}</div>
		</div>

		<div class="navi navi-bold navi-hover navi-link-rounded ms-3">
			<div class="navi-item {{ activemenu('profile.index') }} mb-3">
				<a href="{{ route('profile.index') }}" class="navi-link py-4 {{ activemenu('profile.index','active') }}">
					<span data-feather="disc" class="feather-sm me-1"></span> <span class="navi-text font-size-lg">Profile</span>
				</a>
			</div>
			<div class="navi-item {{ activemenu('profile.account-information') }} mb-3 ">
				<a href="{{ route('profile.account-information') }}" class="navi-link py-4 {{ activemenu('profile.account-information','active') }}">
					<span data-feather="disc" class="feather-sm me-1"></span> <span class="navi-text font-size-lg">Change Password</span>
				</a>
			</div>
		</div>
		<hr class="my-0" />
	</div>
</div>