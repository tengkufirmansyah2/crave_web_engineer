@extends('components.template')
@section('title',__($title))
@section('content')
<div class="container-fluid p-0">
	<div class="row">
		@include('profile.menus.index')

		<div class="col-md-8 col-xl-9">
			<form id="form">
				<input type="hidden" name="_method" value="POST">
				@csrf
				<div class="card card-custom">
					<div class="card-header">
						<div class="card-title align-items-start flex-column">
							<h5 class="card-title mb-0">Change Password</h5>
						</div>
						<div class="card-toolbar">
							<button type="submit" id="submit" class="btn btn-success mr-2">Save</button>
						</div>
					</div>
					<div class="card-body h-100">
						<div class="form-group row">
							<label class="col-xl-3 col-lg-3 col-form-label">Email</label>
							<div class="col-lg-9 col-xl-6">
								<input class="form-control form-control-lg" disabled value="{{ auth()->user()->email }}" />
							</div>
						</div>
						<div class="form-group row">
							<label class="col-xl-3 col-lg-3 col-form-label">New Password</label>
							<div class="col-lg-9 col-xl-6">
                                <input class="form-control" type="password" name="password" placeholder="Enter your password" />
							</div>
						</div>
						<div class="form-group row">
							<label class="col-xl-3 col-lg-3 col-form-label">Confirm Password</label>
							<div class="col-lg-9 col-xl-6">
                                <input class="form-control" type="password" name="password_confirmation" autocomplete="off" value="" placeholder="Confirm Password" />
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection
@section('script')
<script>
    const fv = FormValidation.formValidation(
        document.getElementById('form'),{
            fields: {
                email: {
                    validators: {
                        emailAddress:{
                            message: 'Invalid email!'
                        },
                        notEmpty: {
                            message: 'Email is required!'
                        }
                    }
                },
                password: {
                    validators: {
                        notEmpty: {
                            message: 'Your current password is required!'
                        },
                        stringLength: {
                            min:6,
                            message: 'Minimun is 6 characters.'
                        }
                    }
                },
                password_confirmation: {
                    validators: {
                        identical: {
                            compare: function () {
                                return form.querySelector('[name="password"]').value;
                            },
                            message: 'The password and its confirm are not the same',
                        },
                    },
                },
            },
            plugins:{
                trigger: new FormValidation.plugins.Trigger(),
                // Bootstrap Framework Integration
                bootstrap: new FormValidation.plugins.Bootstrap(),
                // Validate fields when clicking the Submit button
                SubmitButton: new FormValidation.plugins.SubmitButton()
            }
        }
    ).on('core.form.valid', function() {
        var formData = $("#form").serialize();
        // showLoading();
        $.post("{{ route('app.profile.account-information.update') }}", formData)
        .done(function(data){
            Swal.fire("Berhasil!", "{{ 'Berhasil merubah email anda!' }}", "success");
            fv.resetForm(true);
            hideLoading();
            location.reload();
        }).fail(function(xhr,status,error){
            var errs = '<ul style="display:block">'
            if(xhr.status == 422){
                p = xhr.responseJSON.errors;
                for(var key in p){
                    p[key].forEach(err => {
                        errs += '<li><label class="error">'+err+'</label></li>'
                    })
                }
                errs += '</ul>';
            }else{
                errs += '<li><label class="error">'+xhr.responseJSON.message+'</label></li>';
            }
            $('.validation-message').html(errs)
            hideLoading()
        });
    });
</script>
@endsection
