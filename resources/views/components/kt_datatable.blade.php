<div class="card-header">
	<div class="card-title">
		<span class="card-icon"><i class="flaticon-notepad text-primary"></i></span>
		<h3 class="card-label">{{$title}}</h3>
	</div>
	@if($addNew == true)
	<div class="card-toolbar">
		<a href="{{url($url)}}/create" class="btn btn-primary font-weight-bolder">
			<span class="svg-icon svg-icon-md">
				<i class="fas fa-plus-circle"></i>
			</span>	
			{{$title_add}}
		</a>
	</div>
	@endif
</div>
<div class="card-body">
	<div class="mb-7">
		<div class="row align-items-center">
			<div class="col-lg-9 col-xl-8">
				<div class="row align-items-center">
					<div class="col-md-4 my-2 my-md-0">
						<div class="input-icon">
							<input type="text" class="form-control" placeholder="Search..." id="kt_datatable_search_query" />
							<span><i class="flaticon2-search-1 text-muted"></i></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="datatable datatable-bordered datatable-head-custom" id="kt_datatable"></div>
</div>