<div class="p-0">
  <h1 class="display-5 fw-bold">Welcome to Maxx's Co</h1>
  <p class="lead">Maxx's Co was created and designed to make it easier for our customers to make transactions on the services we have provided.</p>
  <p class="lead">Maxx's Co will continue to grow, along the development journey we apologize if there are still services that you need and we cannot provide. Don't hesitate to give us input regarding the services that Maxx's Co should provide for your needs and convenience.</p>
  <p class="lead pt-5">
    <strong>Contact Us</strong><br>
    Lippo Kuningan Building, 19<sup>th</sup> Floor, Unit B<br>
    Jl. HR. Rasuna Said, Kav. B12, Karet Kuningan, Setiabudi<br>
    Jakarta Selatan, DKI Jakarta, 12920<br>
    Telp. <a target="_blank" href="tel:+622150813027">+62 21 5081 3027</a><br>
    Mobile. <a target="_blank" href="https://wa.me/+6281228229384">+62 812 2822 9384</a><br>
    Email. <a target="_blank" href="mailto:hello@maxxsgroup.com">hello@maxxsgroup.com</a>
  </p>
</div>