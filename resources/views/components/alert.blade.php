@if(Session::has('alert'))
<div class="alert alert-danger alert-dismissible fade show" role="alert">
    <div class="alert-message">
        {{ Session::get('message') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
</div>
@elseif(Session::has('message'))

<div class="alert alert-{{Session::get('alert-class')}} alert-dismissible fade show" role="alert">
    <div class="alert-icon">
        <i class="flaticon-questions-circular-button"></i>
    </div>
    <div class="alert-text">
        {{Session::get('message')}}
    </div>
    <div class="alert-close">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true"><i class="ki ki-close"></i></span>
        </button>
    </div>
</div>
@endif
@if ($errors->any())
<div class="alert alert-warning alert-dismissible fade show" role="alert">
    <div class="alert-text">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </div>
  <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif