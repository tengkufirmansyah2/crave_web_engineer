<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Responsive Admin &amp; Dashboard Template based on Bootstrap 5">
	<meta name="author" content="Maxx's Group International">
	<meta name="keywords" content="Maxx's Co">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="shortcut icon" href="{{ URL::asset('/').app_setting()['App_Favico']['valueField'] }}" />

	<title>@yield('title') | {{ app_setting()['App_Name']['valueField'] }}</title>

	{{-- <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet"> --}}
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet">
	<link href="{{ URL::asset('assets/css/app.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ URL::asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ URL::asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ URL::asset('assets/plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ URL::asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ URL::asset('assets/css/all.min.css') }}" rel="stylesheet" type="text/css" />
	@yield('style')
	<style>
			body {
					/* font-family: 'Nunito', sans-serif; */
					font-family: 'Open Sans', sans-serif;
			}
			h1.stat-number {
				font-size: 3rem;
			}
			.marker {
				border: solid 1px #000;
			}
	</style>
</head>

<body>
	<div class="wrapper">
		<nav id="sidebar" class="sidebar js-sidebar">
			@include('components.menu')
		</nav>

		<div class="main">
			@include('components.menu-header')

			<main class="content">
				@yield('content')
			</main>

			<footer class="footer">
				@include('components.footer')
			</footer>
		</div>
	</div>

	@include('components.loading')
	<script>
		var KTAppSettings = {
			"breakpoints": {
				"sm": 576,
				"md": 768,
				"lg": 992,
				"xl": 1200,
				"xxl": 1200
			},
			"font-family": "Open Sans"
		};
	</script>
	<script src="{{ URL::asset('assets/plugins/global/plugins.bundle.js') }}"></script>
	<script src="{{ URL::asset('assets/plugins/custom/prismjs/prismjs.bundle.js') }}"></script>
	<script src="{{ URL::asset('assets/js/jquery.number.min.js') }}"></script>
	<script src="{{ URL::asset('assets/js/scripts.bundle.js') }}"></script>
	<script src="{{ URL::asset('assets/js/pages/widgets.js') }}"></script>
	<script src="{{ URL::asset('assets/js/custom.js') }}"></script>
	<script src="{{ URL::asset('assets/js/Utils.js') }}"></script>
	<script src="{{ URL::asset('assets/js/app.js') }}"></script>
	@yield('script')
	@yield('script1')
</body>
</html>