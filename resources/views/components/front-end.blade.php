<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Responsive Admin &amp; Dashboard Template based on Bootstrap 5">
	<meta name="author" content="Maxx's Group International">
	<meta name="keywords" content="Maxx's Co">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="shortcut icon" href="{{ URL::asset('/').app_setting()['App_Favico']['valueField'] }}" />

	<title>@yield('title') | {{ app_setting()['App_Name']['valueField'] }}</title>

	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600;700&display=swap" rel="stylesheet">
	<link href="{{ URL::asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ URL::asset('assets/css/front-end.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ URL::asset('assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
	@yield('style')
</head>

<body>

<div class="container">
  <header class="blog-header py-3">
    <div class="row flex-nowrap justify-content-between align-items-center">
      <div class="col-4 pt-1">
        <a class="link-secondary" href="{{url('/subscriptions')}}">Subscribe</a>
      </div>
      <div class="col-4 text-center">
        <a class="blog-header-logo text-dark" href="{{url('/')}}">{{ app_setting()['App_Name']['valueField'] }}</a>
      </div>
      <div class="col-4 d-flex justify-content-end align-items-center">
      	@if(Auth::check())
        <div class="dropdown text-end">
          <a href="#" class="d-block link-dark text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
            {{Auth::user()->name}}
          </a>
          <ul class="dropdown-menu text-small" aria-labelledby="dropdownUser1">
            @if(permissionCheck(app('request'),'dashboard-administrator'))
            <li><a class="dropdown-item" href="{{url('/app/beranda')}}">Dashboard</a></li>
            <li><hr class="dropdown-divider"></li>
            @endif
            <li><a class="dropdown-item" href="{{ route('profile.index') }}">Profile</a></li>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="{{ route('app.auth.logout') }}">Sign out</a></li>
          </ul>
        </div>
        @else
        <a class="btn btn-sm btn-outline-secondary" href="{{ route('login') }}">Login</a>
        @endif
      </div>
    </div>
  </header>
</div>

	@yield('content')

	<footer class="text-muted py-5">
		<div class="container">
			<p class="float-end mb-1">
				<a href="#">Back to top</a>
			</p>
			<p class="mb-1">Album example is &copy; Bootstrap, but please download and customize it for yourself!</p>
			<p class="mb-0">New to Bootstrap? <a href="/">Visit the homepage</a> or read our <a href="">getting started guide</a>.</p>
		</div>
	</footer>

	<script src="{{ URL::asset('assets/plugins/global/plugins.bundle.js') }}"></script>
	<script src="{{ URL::asset('assets/plugins/custom/prismjs/prismjs.bundle.js') }}"></script>
	<script src="{{ URL::asset('assets/js/jquery.number.min.js') }}"></script>
	<script src="{{ URL::asset('assets/js/scripts.bundle.js') }}"></script>
	<script src="{{ URL::asset('assets/js/pages/widgets.js') }}"></script>
	<script src="{{ URL::asset('assets/js/custom.js') }}"></script>
	<script src="{{ URL::asset('assets/js/Utils.js') }}"></script>
	<script src="{{ URL::asset('assets/js/app.js') }}"></script>
	@include('components.loading')
	@yield('script')
</body>
</html>