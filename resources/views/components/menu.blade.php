
<div class="sidebar-content js-simplebar">
    <a class="sidebar-brand" href="{{url('app')}}">
        <img alt="Logo" src="{{ URL::asset('/').app_setting()['App_Logo']['valueField'] }}" class="logo-default" width="100%" />
    </a>

    <ul class="sidebar-nav">
        <li class="sidebar-item {{ Request::is('app/beranda') ? 'active' : '' }}">
            <a class="sidebar-link" href="{{url('app/beranda')}}">
                <i class="align-middle" data-feather="sliders"></i> <span class="align-middle">Dashboard</span>
            </a>
        </li>
        @foreach(master_url('menu utama') as $url)
            <li class="sidebar-item {{ Request::is('app/'.$url->url.'*') ? 'active' : '' }}">
                <a class="sidebar-link" href="{{$url->url == '#' ? 'javascript:;' : '/app/'.$url->url}}">
                    <i class="align-middle" data-feather="{{$url->icon}}"></i> <span class="align-middle">{{$url->name}}</span>
                </a>
            </li>
        @endforeach
        @if(count(master_url('master')))
        <li class="sidebar-header">
            Master
        </li>
        @foreach(master_url('master') as $key => $url)
            @if($url->url != '#')
            <li class="sidebar-item {{ Request::is('app/'.$url->url.'*') ? 'active' : '' }}">
                <a class="sidebar-link" href="{{$url->url == '#' ? 'javascript:;' : '/app/'.$url->url}}">
                    <i class="align-middle" data-feather="{{$url->icon}}"></i> <span class="align-middle">{{$url->name}}</span>
                </a>
            </li>
            @else
            <li class="sidebar-item">
                <a data-bs-target="#master{{$key}}" data-bs-toggle="collapse" class="sidebar-link collapsed" aria-expanded="false">
                    <i class="align-middle" data-feather="{{$url->icon}}"></i> <span class="align-middle">{{$url->name}}</span>
                </a>
                @if($url->children != null)
                <ul id="master{{$key}}" class="sidebar-dropdown list-unstyled collapse" data-bs-parent="#sidebar" style="">
                    @foreach($url->children as $children)
                    <li class="sidebar-item {{ Request::is('app/'.$children->url) ? 'active' : '' }}">
                        <a class="sidebar-link" href="{{url('app').'/'.$children->url}}">{{$children->name}}</a>
                    </li>
                    @endforeach
                </ul>
                @endif
            </li>
            @endif
        @endforeach
        @endif

        @if(count(master_url('settings')))
        <li class="sidebar-header">
            Settings
        </li>
        @foreach(master_url('settings') as $url)
        <li class="sidebar-item {{ Request::is('app/'.$url->url) ? 'active' : '' }}">
            <a class="sidebar-link" href="{{$url->url == '#' ? 'javascript:;' : '/app/'.$url->url}}">
                <i class="align-middle" data-feather="{{$url->icon}}"></i> <span class="align-middle">{{$url->name}}</span>
            </a>
        </li>
        @endforeach
        @endif
    </ul>
</div>