<div class="form-group">
    <label>Name<span class="text-danger">*</span></label>
    <input type="text" class="form-control" name="name" placeholder="Parameter Name" value="{{ isset($data->name) ? $data->name : old('name') }}">
</div>
<div class="form-group">
    <label>Slug<span class="text-danger">*</span></label>
    <input type="text" class="form-control" name="slug" placeholder="Slug" value="{{ isset($data->slug) ? $data->slug : old('slug') }}">
</div>
<div class="form-group">
    <label>Description<span class="text-danger">*</span></label>
    <textarea style="height: 200px" name="desc" class="input-reset ba b--black-20 pa2 mb2 db w-100">
        {{isset($data->desc) ? $data->desc : ''}}
    </textarea>
    <input type="hidden" name="description" id="description">
</div>
<div class="row">
    <div class="form-group col-md-6">
        <label>Version<span class="text-danger">*</span></label>
        <input type="text" class="form-control" name="version" placeholder="1.0.0" value="{{ isset($data->version) ? $data->version : old('version') }}">
    </div>
    <div class="form-group col-md-6">
        <label>Responsive</label>
        <label class="form-check">
            <input class="form-check-input" type="checkbox" name="responsive" value="true" {{isset($data->responsive) ? ($data->responsive == 1 ? 'checked' : '') : ''}}>
            <span class="form-check-label">
                Is this template responsive ?
            </span>
        </label>
    </div>
    <div class="form-group col-md-6">
        <label>Subcription<span class="text-danger">*</span></label>
        <select name="subscription_id" id="subscription_id" class="form-control" required>
            @if(isset($data))
            <option value="{{ $data->subscription_id }}">{{ ucfirst($data->subscription ? $data->subscription->name : '') }}</option>
            @endif
        </select>
    </div>

    <div class="form-group col-md-6">
        <label>Programming Language<span class="text-danger">*</span></label>
        <select name="language_id[]" id="language_id" class="selectpicker form-control" multiple required>
        </select>
    </div>
</div>
<div class="form-group row">
    <label>Image <span class="text-danger">*</span></label>
    <div class="col-lg-9 col-xl-6">
        <div class="image-input image-input-outline" id="kt_image_1">
            <div class="image-input-wrapper" style="background-image: url(//{{ isset($data) ? $data->path.'/'.$data->image : '' }})"></div>
            <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                <i class="fa fa-pen icon-sm text-muted"></i>
                <input type="file" name="image" accept=".png, .jpg, .jpeg"/>
                <input type="hidden" name="image_remove"/>
            </label>

            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                <i class="ki ki-bold-close icon-xs text-muted"></i>
            </span>
        </div>
        <span class="form-text text-muted">Allowed file types:  png, jpg, jpeg.</span>
    </div>
</div>
<div class="form-group">
    <label>File (rar/zip) <span class="text-danger">*</span></label>
    <input type="file" class="form-control" name="file">
</div>  