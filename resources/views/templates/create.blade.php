@extends('components.template')
@section('title',$title)
@section('content')
<div class="container-fluid p-0">
    <div class="row">
        <div class="col-md-12">
            <!--begin::Card-->
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                    <div class="card-toolbar">
                        <div class="example-tools justify-content-center">
                            <a href="{{ route('app.templates.index') }}" class="btn btn-info"><i class="ki ki-long-arrow-back"></i> Back</a>
                        </div>
                    </div>
                </div>
                <form enctype="multipart/form-data" id="form">
                    <input type="hidden" name="_method" value="{{ $edit == true ? 'PATCH' : 'POST' }}">
                    @if($edit == true)
                    {{ method_field('PATCH') }}
                    @endif
                    @csrf
                    <div class="card-body">
                        <div class="validation-message"></div>
                        @include ('components.caution', ['title' => $title])
                        @include ('templates.form', ['formMode' => 'create'])
                    </div>
                    <div class="card-footer">
                        <button type="submit" id="submit" class="btn btn-primary mr-2">{{ $edit == true ? 'Change Data' : 'Add New' }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.7.13/tinymce.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var vallang = 'Programming Language', i;
        var language = $("#language_id")[0];
        const selected = [];
        @if(isset($data))
        @foreach($data->language as $val)
        selected.push("{{$val->id}}");
        @endforeach
        @endif
        $.ajax({
            type: "GET",
            url : "{{url('app/get-params').'/'}}"+vallang,
            success: function(addr){
                language.innerHTML = "<option> </option>"
                for(i = 0; i < addr.Data.length; i++){
                    var option = document.createElement("option");
                    option.value = addr.Data[i].id;
                    if(jQuery.inArray(addr.Data[i].id, selected)) {
                        option.selected = true;
                    } else {
                        option.selected = false;
                    }
                    option.text = addr.Data[i].name;
                    language.add(option);
                }
                $('.selectpicker').selectpicker('refresh')
                return false;
            }
        });

        const fv = FormValidation.formValidation(
            document.getElementById('form'),
            {
                fields: {
                    name: {
                        validators: {
                            notEmpty: {
                                message: 'Nama!'
                            },
                            stringLength: {
                                max:100,
                                message: 'Maximum is 100 characters.'
                            }
                        }
                    },
                    slug: {
                        validators: {
                            notEmpty: {
                                message: 'Slug!'
                            },
                            stringLength: {
                                max:100,
                                message: 'Maximum is 100 characters.'
                            }
                        }
                    },
                    version: {
                        validators: {
                            notEmpty: {
                                message: 'Version!'
                            },
                            stringLength: {
                                max:100,
                                message: 'Maximum is 100 characters.'
                            }
                        }
                    },
                    desc: {
                        validators: {
                            callback: {
                                message: 'Description must be longer than 5 characters',
                                callback: function (value) {
                                    // Get the plain text without HTML
                                    const text = tinyMCE.activeEditor.getContent({
                                        format: 'text',
                                    });

                                    return text.length >= 5;
                                },
                            },
                        },
                    },
                    subscription_id: {
                        validators: {
                            notEmpty: {
                                message: 'Subscription!'
                            }
                        }
                    },
                    language_id: {
                        validators: {
                            notEmpty: {
                                message: 'Language!'
                            }
                        }
                    },
                    image: {
                        validators: {
                            notEmpty: {
                                message: 'Image!'
                            }
                        }
                    },
                    file: {
                        validators: {
                            notEmpty: {
                                message: 'File!'
                            },
                            file: {
                                extension: 'rar',
                                type: 'application/x-rar-compressed,application/zip',
                                message: 'Please choose a RAR / ZIP file',
                            },
                        }
                    },
                },
                plugins:{
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    SubmitButton: new FormValidation.plugins.SubmitButton()
                }
            }
        ).on('core.form.valid', function() {
            $('#description').val(tinyMCE.activeEditor.getContent());
            var formData = $("#form").serialize();
            showLoading();
            $.ajax({
                url: "{{ $edit == true ? route('app.templates.update',[app('request')->template]) : route('app.templates.store') }}",
                type: "POST",
                dataType: "JSON",
                data: new FormData($('#form')[0]),
                processData: false,
                contentType: false,
                success: function (data)
                {
                    Swal.fire("Berhasil!", "{{ $edit == true ? 'Successfully changed data templates!' : 'New templates data added successfully!' }}", "success");
                    hideLoading();
                    window.location.href = "{{ route('app.templates.index') }}";
                },
                error: function (xhr, status, error)
                {
                    var errs = '<ul style="display:block">'
                    if(xhr.status == 422){
                        p = xhr.responseJSON.errors;
                        for(var key in p){
                            p[key].forEach(err => {
                                errs += '<li><label class="error">'+err+'</label></li>'
                            })
                        }
                        errs += '</ul>';
                    }else{
                        errs += '<li><label class="error">'+xhr.responseJSON.message+'</label></li>';
                    }
                    $('.validation-message').html(errs)
                    hideLoading()
                }
            });
        });
        tinymce.init({
            selector: 'textarea',
            branding: false,
            menubar: false,
            setup: function (editor) {
                editor.on('keyup', function () {
                    fv.revalidateField('desc');
                });
            },
        });
    });

    $("#subscription_id").focus(function(){
        var val = 'Subscription', i;
        var y = $(this)[0];
        $.ajax({
            type: "GET",
            url : "{{url('app/get-params').'/'}}"+val,
            success: function(addr){
                y.innerHTML = "<option> </option>"
                for(i = 0; i < addr.Data.length; i++){
                    var option = document.createElement("option");
                    option.value = addr.Data[i].id;
                    option.text = addr.Data[i].name;
                    y.add(option);
                }
                return false;
            }
        });
    });
    var image1 = new KTImageInput('kt_image_1');
</script>
@endsection

