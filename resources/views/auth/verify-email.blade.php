@extends('auth.template')
@section('title','Login')
@section('content')
<div class="text-center mt-4">
    <img src="{{ URL::asset('/').app_setting()['App_Logo_Login']['valueField'] }}" class="img-fluid mb-5" style="max-width: 250px;" alt="maxx's group international"  />
</div>

<div class="card">
    <div class="card-body">
        <div class="m-sm-4">
            <div class="mb-4 text-sm text-gray-600">
                {{ __('Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didn\'t receive the email, we will gladly send you another.') }}
            </div>

            @if (session('status') == 'verification-link-sent')
                <div class="mb-4 font-medium text-sm text-green-600">
                    {{ __('A new verification link has been sent to the email address you provided during registration.') }}
                </div>
            @endif

            <div class="mt-4 flex items-center justify-between">
                <form method="POST" action="{{ route('verification.send') }}">
                    @csrf

                    <div>
                        <button type="submit" class="underline text-sm text-gray-600 hover:text-gray-900">
                            {{ __('Resend Verification Email') }}
                        </button>
                    </div>
                </form>

                <form method="POST" action="{{ route('logout') }}">
                    @csrf

                    <button type="submit" class="underline text-sm text-gray-600 hover:text-gray-900">
                        {{ __('Logout') }}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection