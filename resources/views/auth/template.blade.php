<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Responsive Admin &amp; Dashboard Template based on Bootstrap 5">
	<meta name="author" content="Maxx's Group International">
	<meta name="keywords" content="Maxx's Co">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link rel="shortcut icon" href="img/photos/favicon.jpeg" />

	<title>@yield('title') | {{ app_setting()['App_Name']['valueField'] }}</title>
	
	<link href="{{ URL::asset('assets/css/app.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ URL::asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
	{{-- <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;600&display=swap" rel="stylesheet"> --}}
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600&display=swap" rel="stylesheet">
	<style>
		body {
			font-family: 'Open Sans', sans-serif;
		}
	</style>
</head>

<body>
	<main class="d-flex w-100">
		<div class="container d-flex flex-column">
			<div class="row vh-100">
				<div class="col-sm-10 col-md-8 col-lg-6 mx-auto h-100">
					<div class="align-middle">
						@yield('content')
					</div>
				</div>
			</div>
		</div>
	</main>

	<script src="{{ URL::asset('assets/js/app.js') }}"></script>
	@yield('js')
</body>

</html>