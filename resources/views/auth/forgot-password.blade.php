@extends('auth.template')
@section('title','Login')
@section('content')
<div class="text-center mt-4">
    <img src="{{ URL::asset('/').app_setting()['App_Logo_Login']['valueField'] }}" class="img-fluid mb-5" style="max-width: 250px;" alt="maxx's group international" />
</div>

<div class="card">
    <div class="card-body">
        <div class="m-sm-4">
            {{-- <div class="text-center">
                <h2>Forget Password</h2>
            </div> --}}

            <div class="mb-4 text-sm text-gray-600 lead">
                {{ __('Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.') }}
            </div>

            @if (session('status'))
                <div class="mb-4 font-medium text-sm text-green-600">
                    {{ session('status') }}
                </div>
            @endif

            @include('components.alert')

            <form method="POST" action="{{ route('password.email') }}">
                @csrf

                <div class="block">
                    {{-- <label class="form-label">Email</label> --}}
                    <input class="form-control" type="email" name="email" placeholder="Enter your email" value="{{old('email')}}"/>
                    <small>
                        <a href="{{url('/login')}}">Login?</a><br>
                        <a href="{{url('/register')}}">Create an Account?</a>
                    </small>
                </div>

                <div class="flex items-center justify-end mt-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Reset Password') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection