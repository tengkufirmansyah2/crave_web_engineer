@extends('auth.template')
@section('title','Daftar Akun')
@section('content')
<div class="text-center mt-4">
    <img src="{{ URL::asset('/').app_setting()['App_Logo_Login']['valueField'] }}" class="img-fluid mb-5" style="max-width: 250px;" alt="maxx's group international" />
</div>
<div class="card">
    <div class="card-body">
        <div class="m-sm-4">
            <div class="text-center">
                <h2>Register</h2>
            </div>
            <form class="form" method="POST">
                @csrf
                @include('components.alert')
                <div class="mb-3">
                    <label class="form-label">Name</label>
                    <input class="form-control" type="text" name="name" placeholder="Enter your name" />
                </div>
                <div class="mb-3">
                    <label class="form-label">Email</label>
                    <input class="form-control" type="email" name="email" placeholder="Enter your email" />
                </div>
                <div class="mb-3">
                    <label class="form-label">Password</label>
                    <input class="form-control" type="password" name="password" placeholder="Enter your password" />
                </div>
                <div class="mb-3">
                    <label class="form-label">Confirm Password</label>
                    <input class="form-control form-control-solid " type="password" name="password_confirmation" required autocomplete="off" value="" placeholder="Confirm Password" />
                    <small>
                        <a href="{{url('/login')}}">Have an Account?</a>
                    </small>
                </div>
                <div class="text-center mt-3">
                    <button type="submit" class="btn btn-lg btn-primary">Register</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection