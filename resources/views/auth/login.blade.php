@extends('auth.template')
@section('title','Login')
@section('content')
<div class="text-center my-4">
	<img src="{{ URL::asset('/').app_setting()['App_Logo_Login']['valueField'] }}" class="img-fluid" style="max-width: 250px;" alt="maxx's group international" />
</div>

<div class="card">
	<div class="card-body">
		<div class="m-sm-4">
			<div class="text-center">
				<h2>Login</h2>
			</div>
			<form class="form" method="POST">
				@csrf
				@include('components.alert')
				<div class="mb-3">
					<label class="form-label">Email</label>
					<input class="form-control" type="email" name="email" placeholder="Enter your email" value="{{old('email')}}" />
				</div>
				<div class="mb-3">
					<label class="form-label">Password</label>
					<input class="form-control" type="password" name="password" placeholder="Enter your password" />
				</div>
				<div class="mb-3">
					<span class="small"><a class="text-decoration-none" href="{{url('/forgot-password')}}">Forgot password</a></span>
					<span class="small mx-1">|</span>
					<span class="small"><a class="text-decoration-none" href="{{url('/register')}}">Create account</a></span>
						
				</div>
				<div>
					<label class="form-check">
						<input class="form-check-input" type="checkbox" value="remember-me" name="remember-me" checked>
						<span class="form-check-label">
							Remember me next time
						</span>
					</label>
				</div>
				<div class="text-center mt-3">
					<button type="submit" class="btn btn-primary">Login</button>
        			<a class="btn btn-outline-secondary" href="{{ url('/') }}">Back</a>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection