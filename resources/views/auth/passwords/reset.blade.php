
@extends('auth.template')
@section('title','Login')
@section('content')
<div class="text-center mt-4">
    <img src="{{ URL::asset('/').app_setting()['App_Logo_Login']['valueField'] }}" class="img-fluid mb-5" style="max-width: 250px;" alt="maxx's group international"  />
</div>

<div class="card">
    <div class="card-body">
        <div class="m-sm-4">
            <div class="text-center">
                <h2>Password Reset</h2>
            </div>
            <form method="POST" action="{{ route('password.update') }}">
                @csrf
                @include('components.alert')

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="block">
                    <label class="form-label">Email</label>
                    <input class="form-control" type="email" name="email" placeholder="Enter your email" value="{{old('email', $email)}}" />
                </div>

                <div class="mt-4">
                    <label class="form-label">Password</label>
                    <input class="form-control" type="password" name="password" placeholder="Enter your password" />
                </div>

                <div class="mt-4">
                    <label class="form-label">Confirm Password</label>
                    <input class="form-control" type="password" name="password_confirmation" placeholder="Enter your password confirm" />
                </div>

                <div class="flex items-center justify-end mt-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Reset Password') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection