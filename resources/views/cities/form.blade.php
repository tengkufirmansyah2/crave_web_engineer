<div class="form-group">
    <label>Country<span class="text-danger">*</span></label>
    <select name="country_id" id="country_id" class="form-control" required>
        @if($edit == false)
        <option value="">-- Select Country --</option>
        @endif
        @foreach($countries as $ct)
        <option value="{{ $ct->id }}" {{ $edit == true ? ($data->country_id == $ct->id ? 'selected' : '') : '' }} {{ old('country_id') == $ct->id ? 'selected' : '' }}>{{ ucfirst($ct->name) }}</option>
        @endforeach
    </select>
</div>
<div class="form-group">
    <label>State<span class="text-danger">*</span></label>
    <select name="state_id" id="state_id" class="form-control" required>
        @if($edit == true)
        <option value="{{ $data->state_id }}">{{ ucfirst($data->state->name) }}</option>
        @endif
    </select>
</div>
<div class="form-group">
    <label>Nama<span class="text-danger">*</span></label>
    <input type="text" class="form-control" name="name" placeholder="State Name" value="{{ isset($data->name) ? $data->name : old('name') }}">
</div>