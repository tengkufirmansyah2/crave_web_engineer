@extends('components.template')
@section('title',$title)
@section('content')
<div class="container-fluid p-0">
    <div class="row">
        <div class="col-md-12">
            <!--begin::Card-->
            <div class="card card-custom gutter-b example example-compact">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>
                    <div class="card-toolbar">
                        <div class="example-tools justify-content-center">
                            <a href="{{ route('app.countries.index') }}" class="btn btn-info"><i class="ki ki-long-arrow-back"></i> Back</a>
                        </div>
                    </div>
                </div>
                <form id="form">
                    <input type="hidden" name="_method" value="{{ $edit == true ? 'PATCH' : 'POST' }}">
                    @if($edit == true)
                    {{ method_field('PATCH') }}
                    @endif
                    @csrf
                    <div class="card-body">
                        <div class="validation-message"></div>
                        @include ('components.caution', ['title' => $title])
                        @include ('countries.form', ['formMode' => 'create'])
                    </div>
                    <div class="card-footer">
                        <button type="submit" id="submit" class="btn btn-primary mr-2">{{ $edit == true ? 'Change Data' : 'Add New' }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        const fv = FormValidation.formValidation(
            document.getElementById('form'),
            {
                fields: {
                    name: {
                        validators: {
                            notEmpty: {
                                message: 'Nama!'
                            },
                            stringLength: {
                                max:50,
                                message: 'Maximum is 50 characters.'
                            }
                        }
                    }
                },
                plugins:{
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    SubmitButton: new FormValidation.plugins.SubmitButton()
                }
            }
        ).on('core.form.valid', function() {
            var formData = $("#form").serialize();
            showLoading();
            $.post('{{ $edit == true ? route('app.countries.update',[app('request')->country])  : route('app.countries.store') }}', formData)
            .done(function(data){
                Swal.fire("Berhasil!", "{{ $edit == true ? 'Successfully changed data countries!' : 'New Countries data added successfully!' }}", "success");
                hideLoading();
                window.location.href = "{{ route('app.countries.index') }}";
            }).fail(function(xhr,status,error){
                var errs = '<ul style="display:block">'
                if(xhr.status == 422){
                    p = xhr.responseJSON.errors;
                    for(var key in p){
                        p[key].forEach(err => {
                            errs += '<li><label class="error">'+err+'</label></li>'
                        })
                    }
                    errs += '</ul>';
                }else{
                    errs += '<li><label class="error">'+xhr.responseJSON.message+'</label></li>';
                }
                $('.validation-message').html(errs)
                hideLoading()
            });
        });
    });
</script>
    @endsection

