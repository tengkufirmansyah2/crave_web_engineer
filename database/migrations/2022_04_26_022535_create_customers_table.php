<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('user_id');
            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->string('birth_place', 50);
            $table->date('birth_date');
            $table->uuid('gender_id');
            $table->uuid('religion_id');
            $table->string('email', 100);
            $table->string('mobile', 50);
            $table->string('blood_type', 10);
            $table->string('address');
            $table->uuid('country_id');
            $table->uuid('state_id');
            $table->uuid('city_id');
            $table->string('zip_code', 5);
            $table->string('photo_name');
            $table->string('photo_type', 20);
            $table->string('photo_path', 50);

            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
};
