<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('invoice_id');
            $table->string('va_number', 50);
            $table->string('bank', 50);
            $table->decimal('gross_amount', $precision = 15, $scale = 2);
            $table->uuid('currency_id');
            $table->enum('payment_status', ['Waiting', 'Success', 'Expired', 'Cancelled'])->default('Waiting');
            
            $table->string('snap_token', 36)->nullable();
            $table->string('message')->nullable();

            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment');
    }
};
