<?php
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\AppSettings;
use App\Models\Url;
use App\Models\VisaRequest;
use App\Models\Activities;
use App\Models\Customers;

if (!function_exists('app_setting')) {
    function app_setting(){
            $appSettings = \DB::table('app_settings')
                                    ->where('status','=','0')
                                    ->get()
                                    ->toArray();
            $appSettings = AppSettings::hydrate($appSettings);
            $settings = [];
            foreach($appSettings as $key => $r){
                $settings += [str_replace(".","_",$r->keyField) => $r];
            }
            return $settings;
    }
}


if (!function_exists('kt_datatable')) {
    function kt_datatable($title, $url, $addNew = true, $title_add = 'Add New')
    {
        return view('components.kt_datatable', compact('title', 'url', 'addNew', 'title_add'));
    }
}

if (!function_exists('master_url')) {
    function master_url($title)
    {
        $user = Auth::user();
        $url = Url::select('url.id','url.parent_id','url.url','url.icon','url.position','url.name')
                ->join('url_access','url_access.url_id','url.id')
                ->join('users','users.role_id','url_access.role_id')
                ->where('position','=', $title)
                ->where('users.id', $user->id)
                ->orderBy('order','asc')
                ->whereNull('parent_id')
                ->with('children')
                ->get();
        return $url;
    }
}

if (!function_exists('breadcrumb')) {
    function breadcrumb()
    {
        $breadcrumb = "";
        $path = explode("/", request()->path());
        $last = count($path)-1;
        $link = "";
        foreach ($path as $key => $val) {
            $link .= '/'.$val;
            if ($key == 0) {
                $breadcrumb .= '<li class="breadcrumb-item text-muted"><a href="'.url($link).'" class="text-muted">'.$val.'</a></li>';
            }else{
                $breadcrumb .= '<li class="breadcrumb-item text-muted"><a href="'.url($link).'" class="text-muted">'.$val.'</a></li>';
            }

        }
        return $breadcrumb;
    }
}

if (!function_exists('beautiDate')) {
    function beautiDate($date)
    {
        $date = date('Y-m-d', strtotime($date));
        $ex = explode('-', $date);
        $year = $ex[0];
        $month = beautiMonth($ex[1]);
        $day = $ex[2];
        return $day.' '.$month.' '.$year;
        return $breadcrumb;
    }
}

function beautiMonth($month){
    $list = ['Januari', 'Februari', 'Maret', 'April', 'May', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
    $data = $list[$month-1];
    return $data;
}

function alert($type,$message){
    Session()->flash('alert',$type);
    Session()->flash('message',$message);
}
function ktDatatable($data,$req){
    $alldata = $data;
    $datatable = array_merge(array('pagination' => array(), 'sort' => array(), 'query' => array()), $req);
    // search filter by keywords
    $filter = isset($datatable['query']['generalSearch']) && is_string($datatable['query']['generalSearch']) ? $datatable['query']['generalSearch'] : '';
    if (!empty($filter)) {
        $data = array_filter($data, function ($a) use ($filter) {
            return (boolean)preg_grep("/$filter/i", (array)$a);
        });
        unset($datatable['query']['generalSearch']);
    }
    
    // filter by field query
    $query = isset($datatable['query']) && is_array($datatable['query']) ? $datatable['query'] : null;
    if (is_array($query)) {
        $query = array_filter($query);
        foreach ($query as $key => $val) {
            $data = list_filter($data, array($key => $val));
        }
    }
    $sort = !empty($datatable['sort']['sort']) ? $datatable['sort']['sort'] : 'asc';
    $field = !empty($datatable['sort']['field']) ? $datatable['sort']['field'] : 'id';
    
    $meta = array();
    $page = !empty($datatable['pagination']['page']) ? (int)$datatable['pagination']['page'] : 1;
    $perpage = !empty($datatable['pagination']['perpage']) ? (int)$datatable['pagination']['perpage'] : -1;
    
    $pages = 1;
    $total = count($data); // total items in array
    usort($data, function ($a, $b) use ($sort, $field) {
        if (!isset($a->$field) || !isset($b->$field)) {
            return -1;
        }
    
        if ($sort === 'asc') {
            return $a->$field > $b->$field ? 1 : -1;
        }
    
        return $a->$field < $b->$field ? 1 : -1;
    });
    
    // $perpage 0; get all data
    if ($perpage > 0) {
        $pages = ceil($total / $perpage); // calculate total pages
        $page = max($page, 1); // get 1 page when $_REQUEST['page'] <= 0
        $page = min($page, $pages); // get last page when $_REQUEST['page'] > $totalPages
        $offset = ($page - 1) * $perpage;
        if ($offset < 0) {
            $offset = 0;
        }
    
        $data = array_slice($data, $offset, $perpage, true);
    }
    $meta = [
        "page" => $page,
        "pages" => $pages,
        "perpage" => $perpage,
        "total" => $total
    ];
    return $meta;
}
function pendidikanTerakhir(){
    return [
        'SD',
        'SMP',
        'SMA',
        'SMK',
        'D1',
        'D2',
        'D3',
        'S1',
        'S2',
        'S3'
    ];
}
function slugify($text){
    $text = preg_replace('~[^\pL\d]+~u', '-', $text);
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    $text = preg_replace('~[^-\w]+~', '', $text);
    $text = trim($text, '-');
    $text = preg_replace('~-+~', '-', $text);
    $text = strtolower($text);
    if (empty($text)) {
        return 'n-a';
    }
    return $text;
}
if (!function_exists('H_generateCode')) {
    function H_generateCode()
    {
        $check = VisaRequest::whereYear('created_at', date('Y'))
                        ->orderBy('created_at', 'desc');
        if ($check->first() !== null) {
            $code = $check->first()->code;
            $ex = explode('/', $code);
            $num = (int) $ex[count($ex) - 1];
            $nex = sprintf("%04s", $num+1);
            $code = date('Ym').'/MGI-DOCAPP/'.$nex;
        }else{
            $code = date('Ym').'/MGI-DOCAPP/0001';
        }
        return $code;
    }
}

function permissionCheck($request,$modul){
    if (\App\Providers\PermissionsProvider::has($request,$modul)){
        return true;
    }   
    return false;
}
function transformMoney($val){
    $a = preg_replace("[,]", "", preg_replace("[Rp. ]", "", $val));
    $a = preg_replace("[Rp ]","",$a);
    return str_replace(".", "", $a);
}
function bulan($number){
    if($number == 1){
        return 'Januari';
    }
    if($number == 2){
        return 'Februari';
    }
    if($number == 3){
        return 'Maret';
    }
    if($number == 4){
        return 'April';
    }
    if($number == 5){
        return 'Mei';
    }
    if($number == 6){
        return 'Juni';
    }
    if($number == 7){
        return 'Juli';
    }
    if($number == 8){
        return 'Agustus';
    }
    if($number == 9){
        return 'September';
    }
    if($number == 10){
        return 'Oktober';
    }
    if($number == 11){
        return 'November';
    }
    if($number == 12){
        return 'Desember';
    }
}
function daftarBank(){
    $bank = [
        'Bank BSI',
        'Bank Mandiri',
        'Bank BNI',
        'Bank BTPN',
        'Bank Danamon',
        'Bank Permata',
        'Bank BCA',
        'Bank Maybank',
        'Bank CIMB Niaga',
        'Bank OCBC',
    ];
    return $bank;
}

function activemenu($uri, $output = 'navi-active'){
    if( is_array($uri) ) {
        foreach ($uri as $u) {
            if (Route::is($u)) {
                return $output;
            }
        }
    }else{
        if (Route::is($uri)){
            return $output;
        }
    }
}

function fotoProfil(){
    $data = Customers::where('user_id', Auth::user()->id)->first();
    if(isset($data)){
        return '//'.$data->photo_path.'/'.$data->photo_name;
    }
    return url()->asset('assets/media/avatars/admin.png');
}
function fotoProfilThumbnail(){
    $data = Customers::where('user_id', Auth::user()->id)->first();
    if(isset($data)){
        return '//'.$data->photo_path.'/thumbnail/'.$data->photo_name;
    }
    return url()->asset('assets/media/avatars/admin.png');
}

function checkAccount($val)
{
    $data = User::select('users.*')
                ->join('role','role.id','=','users.role_id')
                ->whereIn('role.name', $val)
                ->get();
    return $data;
}

function notification()
{
    $data = Activities::where('user_id', Auth::user()->id)->where('is_read', 0)->orderBy('created_at','desc')->limit(5)->get();
    return $data;
}

function notificationCount()
{
    $data = Activities::where('user_id', Auth::user()->id)->where('is_read', 0)->orderBy('created_at','desc')->count();
    return $data;
}

function numbToWords($num)
{
    $ones = array(
        0 =>" ",
        1 => "ONE",
        2 => "TWO",
        3 => "THREE",
        4 => "FOUR",
        5 => "FIVE",
        6 => "SIX",
        7 => "SEVEN",
        8 => "EIGHT",
        9 => "NINE",
        10 => "TEN",
        11 => "ELEVEN",
        12 => "TWELVE",
        13 => "THIRTEEN",
        14 => "FOURTEEN",
        15 => "FIFTEEN",
        16 => "SIXTEEN",
        17 => "SEVENTEEN",
        18 => "EIGHTEEN",
        19 => "NINETEEN",
        "014" => "FOURTEEN"
    );
    $tens = array( 
        0 => "ZERO",
        1 => "TEN",
        2 => "TWENTY",
        3 => "THIRTY", 
        4 => "FORTY", 
        5 => "FIFTY", 
        6 => "SIXTY", 
        7 => "SEVENTY", 
        8 => "EIGHTY", 
        9 => "NINETY" 
    ); 
    $hundreds = array( 
        "HUNDRED", 
        "THOUSAND", 
        "MILLION", 
        "BILLION", 
        "TRILLION", 
        "QUARDRILLION" 
    );
    $num = number_format($num,3,".",","); 
    $num_arr = explode(".",$num); 
    $wholenum = $num_arr[0]; 
    $decnum = $num_arr[1]; 
    $whole_arr = array_reverse(explode(",",$wholenum)); 
    krsort($whole_arr,1); 
    $rettxt = ""; 
    foreach($whole_arr as $key => $i){
        while(substr($i,0,1)=="0")
            $i=substr($i,1,5);
        if($i < 20){
            if ($key == 1 && $i == 1) {
                $rettxt .= "Se";
            }else{
                $rettxt .= $ones[(int)$i]; 
            }
        }elseif($i < 100){ 
            if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)]; 
            if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)]; 
        }elseif($i < 200){ 
            if(substr($i,0,1)!="0")  $rettxt .= "Seratus";  
            if(substr($i,1,2) < 20){
                if (substr($i,1,2) < 10) {
                    if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)]; 
                }else{
                    if(substr($i,1,2)!="0")$rettxt .= " ".$ones[substr($i,1,2)]; 
                }
            }else{
                if(substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)]; 
                if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)]; 
            }
        }else{ 
            if(substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0]; 
            if(substr($i,1,2) < 20){
                if (substr($i,1,2) < 10) {
                    if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)]; 
                }else{
                    if(substr($i,1,2)!="0")$rettxt .= " ".$ones[substr($i,1,2)]; 
                }
            }else{
                if(substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)]; 
                if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)]; 
            }
        }
        if($key > 0 && $i != 0){ 
            if ($key == 1 && $i == 1 ) {
                $rettxt .= "Ribu"." "; 
            }else{
                $rettxt .= " ".$hundreds[$key]." "; 
            }
        }
    } 
    if($decnum > 0){
        $rettxt .= " point ";
        if($decnum < 20){
            $rettxt .= $ones[$decnum];
        }elseif($decnum < 100){
            $rettxt .= $tens[substr($decnum,0,1)];
            $rettxt .= " ".$ones[substr($decnum,1,1)];
        }elseif($decnum < 200){
            $rettxt .= "Seratus";
            $rettxt .= " ".$tens[substr($decnum,1,1)];
            $rettxt .= " ".$ones[substr($decnum,2,1)];
        }else{ 
            $rettxt .= $ones[substr($decnum,0,1)]." ".$hundreds[0];
            $rettxt .= " ".$tens[substr($decnum,1,1)];
            $rettxt .= " ".$ones[substr($decnum,2,1)];
        } 
    }
    return $rettxt;
}