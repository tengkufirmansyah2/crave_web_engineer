<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Providers\PermissionsProvider;

class Permission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next,$modul){
        if (!PermissionsProvider::has($request,$modul)){
            alert('danger','You do not have permission to this page!');
            return redirect('/app');
        }
        return $next($request);
    }
}
