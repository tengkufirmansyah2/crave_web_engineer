<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TemplatesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100',
            'slug' => 'required|max:100',
            'description' => 'required',
            'version' => 'required|max:100',
            'responsive' => 'required',
            'subscription_id' => 'required',
            'language_id' => 'required',
            'image' => 'required',
            'file' => 'required',
        ];
    }
    public function messages(){
        return [
            'name.required' => 'name is required',
            'slug.required' => 'slug is required',
            'description.required' => 'description is required',
            'version.required' => 'version is required',
            'responsive.required' => 'responsive is required',
            'subscription_id.required' => 'subscription is required',
            'language_id.required' => 'language is required',
            'image.required' => 'image is required',
            'file.required' => 'file is required',
        ];
    }
}
