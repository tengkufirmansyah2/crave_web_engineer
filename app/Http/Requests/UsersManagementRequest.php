<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsersManagementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //berarti dalam edit
        if($this->user){
            return [
                'name' => 'nullable|max:50',
                'email' => 'required|email|unique:users,email,'.$this->user.'|max:225',
                'role_id' => 'required|exists:role,id',
                'password' => 'nullable|min:6',
                'password_confirm' => 'nullable|same:password',
            ];
        }
        return [
            'name' => 'nullable|max:50',
            'email' => 'required|email|unique:users,email,'.$this->user.'|max:225',
            'role_id' => 'required|exists:role,id',
            'password' => 'required|min:6',
            'password_confirm' => 'required|same:password',
        ];
    }
    public function messages(){
        return [
            'name.max' => 'Maksimal karakter nama akun adalah 50!',
            'email.required' => 'Email wajib diisi!',
            'email.email' => 'Email tidak sesuai dengan format!',
            'email.unique' => 'Email sudah digunakan!',
            'email.max' => 'Maksimal karakter email adalah 255!',
            'role_id.required' => 'Role wajib diisi!',
            'role_id.exists' => 'Role tidak tersedia!',
            'password.required' => 'Password wajib diisi!',
            'password.min' => 'Minimal karakter password adalah 6!',
            'password_confirm' => 'Password Konfirmasi wajib diisi!',
            'password_confirm.same' => 'Password tidak sama!',
        ];
    }
}
