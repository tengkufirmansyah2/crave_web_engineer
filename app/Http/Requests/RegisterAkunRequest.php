<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterAkunRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|min:6|alpha_dash|unique:users,username,'.$this->id,
            'password' => 'required|min:6',
            'password_confirm' => 'same:password',
        ];
    }
    public function messages(){
        return [
            'username.min' => 'Karakter minimal username adalah 6 karakter!',
            'username.required' => 'Username wajib diisi!',
            'username.alpha_dash' => 'Username hanya boleh berupa Huruf,Angka,Underscore!',
            'username.unique' => 'Username sudah digunakan sebelumnya!',
            'password.required' => 'Password Wajib diisi!',
            'password.min' => 'Password minimal 6 karakter!',
            'password_confirm.same' => 'Konfirmasi password tidak sesuai!'
        ];
    }
}
