<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UrlRequest;
use App\Http\Resources\UrlCollection;
use App\Providers\PermissionsProvider;
use App\Models\Url;
use DB, Session;
class UrlController extends Controller
{
    public function __construct()
    {
        $this->title = "Url";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (PermissionsProvider::has($request, 'url-view')){
            $title = $this->title;

            if($request->ajax()){
                $url = Url::with('parent')->get()->toArray();
                $kt = ktDatatable($url,$request->all());
                $request->meta = $kt;
                return response()->json(new UrlCollection($url));
            }
            return view('settings.url.index', compact('title'));
        }else{
            Session::flash('message', 'Anda tidak memiliki akses ke '.$this->title.'!'); 
            Session::flash('alert-class', 'alert-danger'); 
            return redirect('/app');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (PermissionsProvider::has($request, 'url-create')){
            $title = 'Tambah '.$this->title.' Baru';
            $parent = Url::where('parent_id', null)->where('position','!=','settings')->get();
            $edit = false;
            return view('settings.url.create',compact('edit','title','parent'));
        }else{
            Session::flash('message', 'Anda tidak memiliki akses ke '.$this->title.'!'); 
            Session::flash('alert-class', 'alert-danger'); 
            return redirect('/app');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UrlRequest $request)
    {
        if (PermissionsProvider::has($request, 'url-create')){
            if($request->ajax()){
                DB::beginTransaction();
                try {
                    Url::create($request->all());
                    DB::commit();
                    return response()->json([
                        'status' => 'success',
                        'message' =>  ''
                    ]);
                } catch (\Exception $e) {
                    DB::rollback();
                    return response()->json([
                        'status' => 'error',
                        'message' =>  $e->getMessage()
                    ],500);
                }
            }
        }else{
            Session::flash('message', 'Anda tidak memiliki akses ke '.$this->title.'!'); 
            Session::flash('alert-class', 'alert-danger'); 
            return redirect('/app');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if (PermissionsProvider::has($request, 'url-edit')){
            $title = 'Edit '.$this->title;
            $parent = Url::where('parent_id', null)->where('position','!=','settings')->get();
            $edit = true;
            $url = Url::where('id',$id)->firstOrFail();
            return view('settings.url.edit',compact('edit','url','title','parent'));
        }else{
            Session::flash('message', 'Anda tidak memiliki akses ke '.$this->title.'!'); 
            Session::flash('alert-class', 'alert-danger'); 
            return redirect('/app');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (PermissionsProvider::has($request, 'url-update')){
            DB::beginTransaction();
            try {
                $url = Url::find($id)->update($request->all());
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json([
                    'status' => 'error',
                    'message' =>  $e->getMessage()
                ],500);
            }
        }else{
            Session::flash('message', 'Anda tidak memiliki akses ke '.$this->title.'!'); 
            Session::flash('alert-class', 'alert-danger'); 
            return redirect('/app');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (PermissionsProvider::has($request, 'url-edit')){
            if($request->ajax()){
                DB::beginTransaction();
                try {
                    Url::where('id',$id)->delete();
                    DB::commit();

                    $code = 200;
                    $message = 'Delet '.$this->title.' Success!';
                    $status = 'success';
                    
                    return response()->json([
                                'code'      => $code,
                                'message'   => $message
                            ]);

                } catch (\Exception $e) {
                    DB::rollback();
                    $code = 500;
                    $message = 'Delet '.$this->title.' Failed!';
                    return response()->json([
                                'code'      => $code,
                                'message'   => $message
                            ]);
                }
            }
        }else{
            DB::rollback();
            $code = 402;
            $message = 'Delet '.$this->title.' Failed!';
            return response()->json([
                        'code'      => $code,
                        'message'   => $message
                    ]);
        }
    }
}
