<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RoleRequest;
use App\Http\Resources\DefaultCollection;
use App\Models\User;
use App\Models\Payment;
use App\Models\Invoice;
use App\Models\Subscriptions;
use App\Providers\PermissionsProvider;
use Session, DB, Hash;

class OrdersController extends Controller
{
    public function __construct()
    {
        $this->title = "Orders";

        $this->middleware('permission:orders-view',['only' => 'index']);
        $this->middleware('permission:orders-create',['only' => ['create','store']]);
        $this->middleware('permission:orders-edit',['only' => ['edit']]);
        $this->middleware('permission:orders-update',['only' => 'update']);
        $this->middleware('permission:orders-delete',['only' => 'destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = $this->title;
        if($request->ajax()){
            $data = Payment::with('invoice','invoice.subscription','invoice.customer','createdByUser','updatedByUser')->get()->toArray();
            $kt = ktDatatable($data,$request->all());
            $request->meta = $kt;
            return response()->json(new DefaultCollection($data));
        }
        return view('orders.index', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $title = 'Edit '.$this->title;
        $edit = true;
        $data = Payment::with('invoice','invoice.subscription','invoice.customer','createdByUser','updatedByUser')->where('id',$id)->firstOrFail();
        // return $data;
        return view('orders.create',compact('edit','data','title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {

            $check = Payment::select('payment.*', 'invoice.subscription_id', 'invoice.customer_id')
                                ->join('invoice','invoice.id','=','payment.invoice_id')
                                ->where('payment.id', $id)
                                ->first();

            $data = Payment::find($id);
            $data->payment_status = $request['status'];
            $data->save();
            
            if($data->payment_status == 'Success'){
                $invoice = Invoice::find($data->invoice_id);
                $invoice->status = 'Paid';
                $invoice->save();

                $invoice = New Subscriptions;
                $invoice->subscription_id = $check->subscription_id;
                $invoice->customer_id = $check->customer_id;
                $invoice->start_date = date('Y-m-d');
                $invoice->end_date = date("Y-m-d", strtotime("+1 month"));
                $invoice->save();
            }else if($data->payment_status == 'Cancelled' || $data->payment_status == 'Expired'){
                $invoice = Invoice::find($data->invoice_id);
                $invoice->status = 'Failed';
                $invoice->save();
            }

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'message' =>  $e->getMessage()
            ],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            DB::beginTransaction();
            try {
                Countries::where('id',$id)->delete();
                DB::commit();

                $code = 200;
                $message = 'Delet '.$this->title.' Success!';
                $status = 'success';
                
                return response()->json([
                            'code'      => $code,
                            'message'   => $message
                        ]);

            } catch (\Exception $e) {
                DB::rollback();
                $code = 500;
                $message = 'Delet '.$this->title.' Failed!';
                return response()->json([
                            'code'      => $code,
                            'message'   => $message
                        ]);
            }
        }
    }
}
