<?php

namespace App\Http\Controllers;

use DB, Artisan, Session;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;
use App\Providers\PermissionsProvider;

use Illuminate\Http\Request;
use App\Models\AppSettings;

class AppSettingsController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->title = "App Settings";
    }

    public function index(Request $request) {

        $title = $this->title;
        switch ($request->method()) {
            case 'GET':
            if (PermissionsProvider::has($request, 'app-settings-view')){
                $appSettings = AppSettings::select('*')->where('status','=','0')->get();
                $settings = [];
                foreach($appSettings as $key => $r){
                    $settings += [str_replace(".","_",$r->keyField) => $r];
                }
                return view('settings.app_settings.index', compact('title','settings'));
                break;
            }else{
                Session::flash('message', 'Anda tidak memiliki akses ke '.$this->title.'!'); 
                Session::flash('alert-class', 'alert-danger'); 
                return redirect('/app');
            }

            case 'POST':
            if (PermissionsProvider::has($request, 'app-settings-update')){
                $data = $request->all();
                foreach ($data as $key => $val) {
                    if ($key == 'App_Logo') {
                        $name_file = "";
                        if ($request->hasFile('App_Logo'))
                        {
                            $dest = public_path('assets/img/app-settings/');
                            $name = $key.'_'.$request->file('App_Logo')->getClientOriginalName();
                            $request->file('App_Logo')->move($dest, $name);
                            $name_file = 'assets/img/app-settings/'.$name;

                            $keyField = str_replace("_",".",$key);
                            $update['keyField'] = $keyField;
                            $update['valueField'] = $name_file;
                            AppSettings::where('keyField',$keyField)->update($update);
                        }
                    }else if ($key == 'App_Logo_Login') {
                        $name_file = "";
                        if ($request->hasFile('App_Logo_Login'))
                        {
                            $dest = public_path('assets/img/app-settings/');
                            $name = $key.'_'.$request->file('App_Logo_Login')->getClientOriginalName();
                            $request->file('App_Logo_Login')->move($dest, $name);
                            $name_file = 'assets/img/app-settings/'.$name;
                            
                            $keyField = str_replace("_",".",$key);
                            $update['keyField'] = $keyField;
                            $update['valueField'] = $name_file;
                            AppSettings::where('keyField',$keyField)->update($update);
                        }
                    }else if ($key == 'App_Login_Cover') {
                        $name_file = "";
                        if ($request->hasFile('App_Login_Cover'))
                        {
                            $dest = public_path('assets/img/app-settings/');
                            $name = $key.'_'.$request->file('App_Login_Cover')->getClientOriginalName();
                            $request->file('App_Login_Cover')->move($dest, $name);
                            $name_file = 'assets/img/app-settings/'.$name;
                            
                            $keyField = str_replace("_",".",$key);
                            $update['keyField'] = $keyField;
                            $update['valueField'] = $name_file;
                            AppSettings::where('keyField',$keyField)->update($update);
                        }
                    }else if ($key == 'App_Favico') {
                        $name_file = "";
                        if ($request->hasFile('App_Favico'))
                        {
                            $dest = public_path('assets/img/app-settings/');
                            $name = $key.'_'.$request->file('App_Favico')->getClientOriginalName();
                            $request->file('App_Favico')->move($dest, $name);
                            $name_file = 'assets/img/app-settings/'.$name;
                            
                            $keyField = str_replace("_",".",$key);
                            $update['keyField'] = $keyField;
                            $update['valueField'] = $name_file;
                            AppSettings::where('keyField',$keyField)->update($update);
                        }
                    }else{
                        $keyField = str_replace("_",".",$key);
                        $update['keyField'] = $keyField;
                        $update['valueField'] = $val;
                        AppSettings::where('keyField',$keyField)->update($update);
                    }
                }
                Session::flash('message', 'Update '.$this->title.' Success!'); 
                Session::flash('alert-class', 'alert-success'); 
                return redirect('/app/app-settings');
                break;
            }else{
                Session::flash('message', 'Anda tidak memiliki akses ke '.$this->title.'!'); 
                Session::flash('alert-class', 'alert-danger'); 
                return redirect('/app');
            }

            default:
            dd("none");
            break;
        }
    }

}
