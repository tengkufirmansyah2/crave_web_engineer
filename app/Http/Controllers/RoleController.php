<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RoleRequest;
use App\Http\Resources\RoleCollection;
use App\Models\Role;
use App\Models\Url;
use App\Models\UrlAccess;
use App\Models\Permissions;
use App\Models\RolePermissions;
use App\Providers\PermissionsProvider;
use Session, DB;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->title = "Roles";

        $this->middleware('permission:role-view',['only' => 'index']);
        $this->middleware('permission:role-edit',['only' => ['edit', 'destroy']]);
        $this->middleware('permission:role-update',['only' => 'update']);
        $this->middleware('permission:role-create',['only' => ['create','store']]);
        $this->middleware('permission:role-config',['only' => ['permission','menu']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = $this->title;
        if($request->ajax()){
            $role = Role::get()->toArray();
            $kt = ktDatatable($role,$request->all());
            $request->meta = $kt;
            return response()->json(new RoleCollection($role));
        }
        return view('settings.role.index', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = 'Tambah '.$this->title.' Baru';
        $edit = false;
        return view('settings.role.create',compact('edit','title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        if($request->ajax()){
            DB::beginTransaction();
            try {
                Role::create($request->all());
                DB::commit();
                return response()->json([
                    'status' => 'success',
                    'message' =>  ''
                ]);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json([
                    'status' => 'error',
                    'message' =>  $e->getMessage()
                ],500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $title = 'Edit '.$this->title;
        $edit = true;
        $role = Role::where('id',$id)->firstOrFail();
        return view('settings.role.edit',compact('edit','role','title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $role = Role::find($id)->update($request->all());
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'message' =>  $e->getMessage()
            ],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            DB::beginTransaction();
            try {
                Role::where('id',$id)->delete();
                DB::commit();

                $code = 200;
                $message = 'Delet '.$this->title.' Success!';
                $status = 'success';
                
                return response()->json([
                            'code'      => $code,
                            'message'   => $message
                        ]);

            } catch (\Exception $e) {
                DB::rollback();
                $code = 500;
                $message = 'Delet '.$this->title.' Failed!';
                return response()->json([
                            'code'      => $code,
                            'message'   => $message
                        ]);
            }
        }
    }

    public function permission(Request $request, $id)
    {
        $title = 'Configuration Permissions '.$this->title;
        switch ($request->method()) {
            case 'GET':
                $config = Permissions::select('permissions.*','role_permissions.role_id','role_permissions.id as rp_id')
                            ->leftJoin('role_permissions', 'role_permissions.permission_id',DB::raw('permissions.id AND role_permissions.role_id = "' . $id.'"'))
                            ->orderBy('permissions.name','asc')
                            ->get();
                return view('settings.role.config.permission', compact('title','config'));
            break;

            case 'POST':
                DB::beginTransaction();
                try {
                    $data = $request->all();
                    $delete = RolePermissions::where('role_id', $id)->delete();
                    foreach ($data['akses'] as $key => $val) {
                        RolePermissions::create([
                            'permission_id' => $val,
                            'role_id' => $id,
                        ]);
                    }
                    DB::commit();
                    Session::flash('message', 'Add Permissions '.$this->title.' Success!'); 
                    Session::flash('alert-class', 'alert-success'); 
                    return redirect('app/role');
                } catch (\Exception $e) {
                    DB::rollback();
                    Session::flash('message', 'Add Permissions '.$this->title.' Failed!'); 
                    Session::flash('alert-class', 'alert-danger'); 
                    return redirect('app/role');
                }
            break;
        };
    }

    public function menu(Request $request, $id)
    {
        $title = 'Configuration Menus '.$this->title;
        switch ($request->method()) {
            case 'GET':
                $config = Url::select('url.*','url_access.role_id')->with('parent')
                        ->leftJoin('url_access', 'url_access.url_id',DB::raw('url.id AND url_access.role_id = "'.$id.'"'))
                        ->orderBy('position','asc')
                        ->orderBy('order','asc')
                        ->orderBy('parent_id','asc')
                        ->get();
                return view('settings.role.config.menu', compact('title','config'));
            break;

            case 'POST':
                DB::beginTransaction();
                try {
                    $data = $request->all();
                    $delete = UrlAccess::where('role_id', $id)->delete();
                    foreach ($data['akses'] as $key => $val) {
                        UrlAccess::create([
                            'url_id' => $val,
                            'role_id' => $id,
                        ]);
                    }
                    DB::commit();
                    Session::flash('message', 'Add Menus '.$this->title.' Success!'); 
                    Session::flash('alert-class', 'alert-success'); 
                    return redirect('app/role');
                } catch (\Exception $e) {
                    DB::rollback();
                    Session::flash('message', 'Add Menus '.$this->title.' Failed!'); 
                    Session::flash('alert-class', 'alert-danger'); 
                    return redirect('app/role');
                }
            break;
        };
    }
}
