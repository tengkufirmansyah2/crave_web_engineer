<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\DefaultCollection;
use App\Http\Requests\CustomersRequest;
use App\Models\User;
use App\Models\Customers;
use App\Providers\PermissionsProvider;
use Session, DB, Hash;

class CustomersController extends Controller
{
    public function __construct()
    {
        $this->title = "Customers";

        $this->middleware('permission:customers-view',['only' => 'index']);
        $this->middleware('permission:customers-create',['only' => ['create','store']]);
        $this->middleware('permission:customers-edit',['only' => ['edit']]);
        $this->middleware('permission:customers-update',['only' => 'update']);
        $this->middleware('permission:customers-delete',['only' => 'destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = $this->title;
        if($request->ajax()){
            $data = Customers::with('createdByUser','updatedByUser','country','state','city','user','user.role')->get()->toArray();
            $kt = ktDatatable($data,$request->all());
            $request->meta = $kt;
            return response()->json(new DefaultCollection($data));
        }
        return view('customers.index', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = 'Create '.$this->title;
        $edit = false;
        return view('customers.create',compact('edit','title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomersRequest $request)
    {
        if($request->ajax()){
            DB::beginTransaction();
            try {
                Customers::create($request->all());
                DB::commit();
                return response()->json([
                    'status' => 'success',
                    'message' =>  ''
                ]);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json([
                    'status' => 'error',
                    'message' =>  $e->getMessage()
                ],500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $title = 'Edit '.$this->title;
        $edit = true;
        $data = Customers::where('id',$id)->firstOrFail();
        return view('customers.create',compact('edit','data','title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustomersRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $url = Customers::find($id)->update($request->all());
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'message' =>  $e->getMessage()
            ],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            DB::beginTransaction();
            try {
                Customers::where('id',$id)->delete();
                DB::commit();

                $code = 200;
                $message = 'Delet '.$this->title.' Success!';
                $status = 'success';
                
                return response()->json([
                            'code'      => $code,
                            'message'   => $message
                        ]);

            } catch (\Exception $e) {
                DB::rollback();
                $code = 500;
                $message = 'Delet '.$this->title.' Failed!';
                return response()->json([
                            'code'      => $code,
                            'message'   => $message
                        ]);
            }
        }
    }
}
