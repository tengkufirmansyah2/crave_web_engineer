<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Url;
use App\Models\Countries;
use App\Models\States;
use App\Models\Cities;
use App\Models\Customers;
use App\Models\VisaRequest;
use App\Models\Parameters;
use Auth;
use DB;
class BerandaController extends Controller
{
    public function __construct()
    {
        $this->title = "Beranda";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = $this->title;
        $data = Customers::where('user_id', Auth::user()->id)->first();
        if(permissionCheck(app('request'),'dashboard-administrator')){
            $customers = Customers::count();
            $countries = Countries::count();
            $states = States::count();
            $cities = Cities::count();
        }
        if (isset($data)) {
            if(permissionCheck(app('request'),'dashboard-administrator')){
                return view('beranda.index', compact('title','customers', 'countries', 'states', 'cities'));
            }else{
                return view('beranda.index', compact('title'));
            }
        }
        alert('danger','Please complete your profile!');
        return redirect("profile");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getCities($id){
        $data = DB::table('cities')->select('id as Kode','name as Nama')->where('province_id',$id)->get();
        return response()->json(['Hasil'=>$data]);
    }

    public function getParams($id){
        $data = Parameters::select('id','name','value')
                        ->where('group',$id)
                        ->orderBy('name','asc')
                        ->get();
        return response()->json(['Data'=>$data]);
    }
}
