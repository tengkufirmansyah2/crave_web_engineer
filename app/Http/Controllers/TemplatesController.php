<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\DefaultCollection;
use App\Http\Requests\TemplatesRequest;
use App\Models\User;
use App\Models\Templates;
use App\Providers\PermissionsProvider;
use Session, DB, Hash, Image;

class TemplatesController extends Controller
{
    public function __construct()
    {
        $this->title = "Templates";

        $this->middleware('permission:templates-view',['only' => 'index']);
        $this->middleware('permission:templates-create',['only' => ['create','store']]);
        $this->middleware('permission:templates-edit',['only' => ['edit']]);
        $this->middleware('permission:templates-update',['only' => 'update']);
        $this->middleware('permission:templates-delete',['only' => 'destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = $this->title;
        if($request->ajax()){
            $data = Templates::with('createdByUser','updatedByUser','subscription')->get()->toArray();
            $kt = ktDatatable($data,$request->all());
            $request->meta = $kt;
            return response()->json(new DefaultCollection($data));
        }
        return view('templates.index', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = 'Create '.$this->title;
        $edit = false;
        return view('templates.create',compact('edit','title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TemplatesRequest $request)
    {
        if($request->ajax()){
            DB::beginTransaction();
            try {

                $data = New Templates;
                $data->name = $request['name'];
                $data->slug = $request['slug'];
                $data->desc = $request['description'];
                $data->version = $request['version'];
                $data->responsive = isset($request['responsive']) ? 1 : 0;
                $data->subscription_id = $request['subscription_id'];
                $data->language_id = implode(',', $request['language_id']);


                $data->path = preg_replace('#^https?://#i', '', url('storage/templates'));

                if(isset($request['image'])){
                    $image = $this->images($request['image']);
                    $data->image = $image;
                };

                if(isset($request['file'])){
                    $file = $this->file($request['file']);
                    $data->file = $file;
                }

                $data->save();

                DB::commit();
                return response()->json([
                    'status' => 'success',
                    'message' =>  ''
                ]);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json([
                    'status' => 'error',
                    'message' =>  $e->getMessage()
                ],500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $title = 'Edit '.$this->title;
        $edit = true;
        $data = Templates::where('id',$id)->with('subscription')->firstOrFail();
        // return $data;
        return view('templates.create',compact('edit','data','title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TemplatesRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = Templates::find($id);
            $data->name = $request['name'];
            $data->slug = $request['slug'];
            $data->desc = $request['description'];
            $data->version = $request['version'];
            $data->responsive = isset($request['responsive']) ? 1 : 0;
            $data->subscription_id = $request['subscription_id'];
            $data->language_id = implode(',', $request['language_id']);


            $data->path = preg_replace('#^https?://#i', '', url('storage/templates'));

            if(isset($request['image'])){
                $image = $this->images($request['image']);
                $data->image = $image;
            };

            if(isset($request['file'])){
                $file = $this->file($request['file']);
                $data->file = $file;
            }

            $data->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'message' =>  $e->getMessage()
            ],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            DB::beginTransaction();
            try {
                Templates::where('id',$id)->delete();
                DB::commit();

                $code = 200;
                $message = 'Delet '.$this->title.' Success!';
                $status = 'success';
                
                return response()->json([
                            'code'      => $code,
                            'message'   => $message
                        ]);

            } catch (\Exception $e) {
                DB::rollback();
                $code = 500;
                $message = 'Delet '.$this->title.' Failed!';
                return response()->json([
                            'code'      => $code,
                            'message'   => $message
                        ]);
            }
        }
    }

    public function file($val)
    {
        $filenamewithextension = $val->getClientOriginalName();
        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
        $extension = $val->getClientOriginalExtension();
  
        $filenametostore = $filename.'_'.time().'.'.$extension;

        $val->storeAs('public/templates', $filenametostore);
        return $filenametostore;
    }

    public function images($val)
    {
        $filenamewithextension = $val->getClientOriginalName();
        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
        $extension = $val->getClientOriginalExtension();
  
        $filenametostore = $filename.'_'.time().'.'.$extension;

        $val->storeAs('public/templates', $filenametostore);
        $val->storeAs('public/templates/thumbnail', $filenametostore);

        $mediumthumbnailpath = public_path('storage/templates/thumbnail/'.$filenametostore);
        $this->createThumbnail($mediumthumbnailpath, $filenametostore, 300, 300);
        return $filenametostore;
    }

    public function createThumbnail($path, $file, $width, $height)
    {
        $img = Image::make($path);
        
        if ($img->width() > $width) { 
            $img->resize($width, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        if ($img->height() > $height) {
            $img->resize(null, $height, function ($constraint) {
                $constraint->aspectRatio();
            }); 
        }

        $img->resizeCanvas($width, $height, 'center', false, '#ffffff');
        $img->save($path);
    }
}
