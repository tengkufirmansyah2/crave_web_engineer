<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UrlRequest;
use App\Http\Resources\DefaultCollection;
use App\Providers\PermissionsProvider;

use App\Http\Requests\CitiesRequest;

use App\Models\MasterImages;

use DB, Session, Auth, ImageThumbnail;

class MasterImagesController extends Controller
{
    public function images($val, $path)
    {
        $filenamewithextension = $val->getClientOriginalName();
        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
        $extension = $val->getClientOriginalExtension();
  
        $filenametostore = $filename.'_'.time().'.'.$extension;

        $val->storeAs('public/'.$path.'', $filenametostore);
        $val->storeAs('public/'.$path.'/thumbnail', $filenametostore);

        $mediumthumbnailpath = public_path('storage/'.$path.'/thumbnail/'.$filenametostore);
        MasterImagesCtrl::createThumbnail($mediumthumbnailpath, 300, 185);
        return $filenametostore;
    }

    public function createThumbnail($path, $width, $height)
    {
        $img = ImageThumbnail::make($path)->resize($width, $height);
        $img->save($path);
    }
}
