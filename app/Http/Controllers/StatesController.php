<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\DefaultCollection;
use App\Http\Requests\StatesRequest;
use App\Models\User;
use App\Models\States;
use App\Models\Countries;
use App\Providers\PermissionsProvider;
use Session, DB, Hash;

class StatesController extends Controller
{
    public function __construct()
    {
        $this->title = "States";

        $this->middleware('permission:states-view',['only' => 'index']);
        $this->middleware('permission:states-create',['only' => ['create','store']]);
        $this->middleware('permission:states-edit',['only' => ['edit']]);
        $this->middleware('permission:states-update',['only' => 'update']);
        $this->middleware('permission:states-delete',['only' => 'destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = $this->title;
        if($request->ajax()){
            $data = States::with('createdByUser','updatedByUser','country')->get()->toArray();
            $kt = ktDatatable($data,$request->all());
            $request->meta = $kt;
            return response()->json(new DefaultCollection($data));
        }
        return view('states.index', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = 'Create '.$this->title;
        $edit = false;
        $countries = Countries::get();
        return view('states.create',compact('edit','countries','title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StatesRequest $request)
    {
        if($request->ajax()){
            DB::beginTransaction();
            try {
                States::create($request->all());
                DB::commit();
                return response()->json([
                    'status' => 'success',
                    'message' =>  ''
                ]);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json([
                    'status' => 'error',
                    'message' =>  $e->getMessage()
                ],500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $title = 'Edit '.$this->title;
        $edit = true;
        $data = States::where('id',$id)->firstOrFail();
        $countries = Countries::get();
        return view('states.create',compact('edit','data','countries','title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StatesRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $url = States::find($id)->update($request->all());
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'message' =>  $e->getMessage()
            ],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            DB::beginTransaction();
            try {
                States::where('id',$id)->delete();
                DB::commit();

                $code = 200;
                $message = 'Delet '.$this->title.' Success!';
                $status = 'success';
                
                return response()->json([
                            'code'      => $code,
                            'message'   => $message
                        ]);

            } catch (\Exception $e) {
                DB::rollback();
                $code = 500;
                $message = 'Delet '.$this->title.' Failed!';
                return response()->json([
                            'code'      => $code,
                            'message'   => $message
                        ]);
            }
        }
    }

    public function getStates($id){
        $data = States::select('id','name')
                        ->where('country_id',$id)
                        ->get();
        return response()->json(['Data'=>$data]);
    }
}
