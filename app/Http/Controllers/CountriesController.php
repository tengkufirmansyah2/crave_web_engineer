<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RoleRequest;
use App\Http\Resources\DefaultCollection;
use App\Http\Requests\CountriesRequest;
use App\Models\User;
use App\Models\Countries;
use App\Providers\PermissionsProvider;
use Session, DB, Hash;

class CountriesController extends Controller
{
    public function __construct()
    {
        $this->title = "Countries";

        $this->middleware('permission:countries-view',['only' => 'index']);
        $this->middleware('permission:countries-create',['only' => ['create','store']]);
        $this->middleware('permission:countries-edit',['only' => ['edit']]);
        $this->middleware('permission:countries-update',['only' => 'update']);
        $this->middleware('permission:countries-delete',['only' => 'destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = $this->title;
        if($request->ajax()){
            $data = Countries::with('createdByUser','updatedByUser')->get()->toArray();
            $kt = ktDatatable($data,$request->all());
            $request->meta = $kt;
            return response()->json(new DefaultCollection($data));
        }
        return view('countries.index', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = 'Create '.$this->title;
        $edit = false;
        return view('countries.create',compact('edit','title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        if($request->ajax()){
            DB::beginTransaction();
            try {
                Countries::create($request->all());
                DB::commit();
                return response()->json([
                    'status' => 'success',
                    'message' =>  ''
                ]);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json([
                    'status' => 'error',
                    'message' =>  $e->getMessage()
                ],500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $title = 'Edit '.$this->title;
        $edit = true;
        $data = Countries::where('id',$id)->firstOrFail();
        return view('countries.create',compact('edit','data','title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CountriesRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $url = Countries::find($id)->update($request->all());
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'message' =>  $e->getMessage()
            ],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            DB::beginTransaction();
            try {
                Countries::where('id',$id)->delete();
                DB::commit();

                $code = 200;
                $message = 'Delet '.$this->title.' Success!';
                $status = 'success';
                
                return response()->json([
                            'code'      => $code,
                            'message'   => $message
                        ]);

            } catch (\Exception $e) {
                DB::rollback();
                $code = 500;
                $message = 'Delet '.$this->title.' Failed!';
                return response()->json([
                            'code'      => $code,
                            'message'   => $message
                        ]);
            }
        }
    }
}
