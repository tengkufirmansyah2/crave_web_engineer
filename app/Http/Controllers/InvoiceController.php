<?php

namespace App\Http\Controllers;

use App\Services\Midtrans\CreateSnapTokenService;
use Ramsey\Uuid\Uuid as Generator;
use Illuminate\Http\Request;
use App\Http\Resources\DefaultCollection;
use App\Models\User;
use App\Models\Customers;
use App\Models\Invoice;
use App\Models\Parameters;
use App\Models\Payment;
use App\Models\Subscriptions;

use App\Providers\PermissionsProvider;
use Session, DB, Hash, Auth, PDF;

class InvoiceController extends Controller
{
    public function __construct()
    {
        $this->title = "Invoice";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function updatePayment(Request $request, $id)
    {
        if($request->ajax()){
            DB::beginTransaction();
            try {
                $check = Payment::select('payment.*')
                                    ->join('invoice','invoice.id','=','payment.invoice_id')
                                    ->where('subscription_id', $id)
                                    ->whereIn('status', ['Waiting', 'Process'])
                                    ->first();
                $data = Payment::find($check->id);
                if($request['va_numbers']){
                    $data->va_number = $request['va_numbers'][0]['va_number'];
                    $data->bank = $request['va_numbers'][0]['bank'];
                }else{
                    $data->va_number = $request['bca_va_number'] ? $request['bca_va_number'] : ($request['bill_key'] ? $request['bill_key'] : (isset($request['bank']) ? $request['bank'] : '-'));
                    $data->bank = $request['payment_type'];
                }

                if($request['transaction_status'] == 'pending'){
                    $data->payment_status = 'Waiting';
                }else if($request['transaction_status'] == 'settlement'){
                    $data->payment_status = 'Success';
                }else if($request['transaction_status'] == 'cancel'){
                    $data->payment_status = 'Cancelled';
                }else if($request['transaction_status'] == 'capture'){
                    if($request['status_code'] == 200){
                        $data->payment_status = 'Success';
                    }else if($request['status_code'] == 201){
                        $data->payment_status = 'Waiting';
                    }
                }else{
                    $data->payment_status = 'Expired';
                }
                
                $data->message = $request['status_message'];
                $data->save();

                if($data->payment_status == 'Waiting'){
                    $invoice = Invoice::find($data->invoice_id);
                    $invoice->status = 'Process';
                    $invoice->save();
                }else if($data->payment_status == 'Success'){
                    $invoice = Invoice::find($data->invoice_id);
                    $invoice->status = 'Paid';
                    $invoice->save();

                    $invoice = New Subscriptions;
                    $invoice->subscription_id = $check->subscription_id;
                    $invoice->customer_id = $check->customer_id;
                    $invoice->start_date = date('Y-m-d');
                    $invoice->end_date = date("Y-m-d", strtotime("+1 month"));
                    $invoice->save();

                }else if($data->payment_status == 'Cancelled' || $data->payment_status == 'Expired'){
                    $invoice = Invoice::find($data->invoice_id);
                    $invoice->status = 'Failed';
                    $invoice->save();
                }

                DB::commit();
                return response()->json([
                    'status' => 'success',
                    'message' =>  ''
                ]);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json([
                    'status' => 'error',
                    'message' =>  $e->getMessage()
                ],500);
            }
        }
    }

    public function payProcess(Request $request)
    {
        DB::beginTransaction();
        try {
            $check = Payment::select('payment.*', 'invoice.subscription_id', 'invoice.customer_id')
                                ->join('invoice','invoice.id','=','payment.invoice_id')
                                ->where('payment.id', $request->order_id)
                                ->first();

            $data = Payment::find($request->order_id);
            if($request['va_numbers']){
                $data->va_number = $request['va_numbers'][0]['va_number'];
                $data->bank = $request['va_numbers'][0]['bank'];
            }else{
                $data->va_number = isset($request['bank']) ? $request['bank'] : '-';
                $data->bank = $request['payment_type'];
            }
            $data->message = $request['status_message'];
            if($request['transaction_status'] == 'pending'){
                $data->payment_status = 'Waiting';
            }else if($request['transaction_status'] == 'settlement'){
                $data->payment_status = 'Success';
            }else if($request['transaction_status'] == 'cancel'){
                $data->payment_status = 'Cancelled';
            }else if($request['transaction_status'] == 'capture'){
                if($request['status_code'] == 200){
                    $data->payment_status = 'Success';
                }else if($request['status_code'] == 201){
                    $data->payment_status = 'Waiting';
                }
            }else{
                $data->payment_status = 'Expired';
            }

            $data->save();
            
            if($data->payment_status == 'Success'){
                $invoice = Invoice::find($data->invoice_id);
                $invoice->status = 'Paid';
                $invoice->save();

                $invoice = New Subscriptions;
                $invoice->subscription_id = $check->subscription_id;
                $invoice->customer_id = $check->customer_id;
                $invoice->start_date = date('Y-m-d');
                $invoice->end_date = date("Y-m-d", strtotime("+1 month"));
                $invoice->save();

            }else if($data->payment_status == 'Cancelled' || $data->payment_status == 'Expired'){
                $invoice = Invoice::find($data->invoice_id);
                $invoice->status = 'Failed';
                $invoice->save();
            }

            DB::commit();
            return response()->json([
                'status' => 'success',
                'message' =>  ''
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'message' =>  $e->getMessage()
            ],500);
        }
    }
}
