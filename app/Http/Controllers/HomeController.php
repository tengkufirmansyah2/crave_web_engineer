<?php

namespace App\Http\Controllers;

use App\Services\Midtrans\CreateSnapTokenService;
use Illuminate\Http\Request;
use App\Models\Templates;
use App\Models\Customers;
use App\Models\Subscriptions;
use App\Models\Payment;
use App\Models\Invoice;
use App\Models\Parameters;
use Auth, DB, Session;
class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = Templates::select('*')->with('subscription')->paginate(6);
        // return $data;
        return view('front-end.home', compact('data'));
    }

    public function detail($id)
    {
        $data = Templates::select('*')->with('subscription')->where('id', $id)->firstOrFail();
        // return $data;
        return view('front-end.detail', compact('data'));
    }

    public function download($id)
    {
        if (Auth::user()) {
            $customer = Customers::where('user_id', Auth::user()->id)->first();
            // dd($customer);
            if (isset($customer)) {
                $data = Templates::select('*')->with('subscription')->where('id', $id)->firstOrFail();

                $check = Subscriptions::select('*')
                                    ->join('parameters','parameters.id','=','subscriptions.subscription_id')
                                    ->where('subscriptions.customer_id', $customer->id)
                                    ->where('end_date','>=', date('Y-m-d'))
                                    ->orderBy(DB::raw('cast(value as unsigned)'), 'desc')
                                    ->first();
                if (isset($check)) {
                    if ((float)$data->subscription->value <= (float)$check->value) {
                        return response()->download(substr($data->path, -17).'/'.$data->file);
                    }
                    Session::flash('message', 'Silahkan upgrade Subscription terlebih dahulu untuk mendownload template ini !'); 
                    Session::flash('alert-class', 'alert-danger'); 
                    return redirect("/subscriptions");
                }else{
                    Session::flash('message', 'Silahkan upgrade Subscription terlebih dahulu untuk mendownload template ini !'); 
                    Session::flash('alert-class', 'alert-danger');
                    return redirect("/subscriptions");
                }
            }else{
                return redirect("/profile");
            }
        }else{
            return redirect("/login");
        }
    }

    public function listSubcriptions()
    {
        $pay = false;
        if (Auth::user()) {
            $data = Customers::where('user_id', Auth::user()->id)->first();
            if (isset($data)) {
                $list = Subscriptions::where('customer_id', $data->id)->with('subscription')->get();
                $order = Payment::select('payment.*', 'parameters.name')
                                ->join('invoice','invoice.id','=','payment.invoice_id')
                                ->join('parameters','parameters.id','=','invoice.subscription_id')
                                ->where('customer_id', $data->id)
                                ->where('payment_status', '=', 'Waiting')
                                ->get();

                $subscriptions = Parameters::where('group','=','Subscription')->where('value', '!=', 0)->orderBy('name','asc')->get();
                return view('front-end.subscriptions', compact('list', 'pay', 'subscriptions', 'order'));
            }
            alert('danger','Please complete your profile!');
            return redirect("/profile");
        }else{
            return redirect("/login");
        }
    }

    public function checkSubcriptions(Request $request, $id)
    {
        if($request->ajax()){
            $data = Subscriptions::select('subscriptions.*','parameters.name','parameters.value')
                            ->join('parameters','parameters.id','=','subscriptions.subscription_id')
                            ->where('end_date', '>=', date('Y-m-d'))
                            ->orderBy('created_at', 'desc')
                            ->first();

            if (isset($data)) {
                $check = Parameters::select('*')->where('id', $id)->first();
                if ((float)$check->value >= (float)$data->value) {
                    return response()->json([
                        'status' => 'success',
                        'message' =>  'Next to payment'
                    ]);
                }else{
                    return response()->json([
                        'status' => 'failed',
                        'message' =>  'the selected package is lower than the currently active package!'
                    ]);
                }
            }else{
                return response()->json([
                    'status' => 'success',
                    'message' =>  'Next to payment'
                ]);
            }
            return ;
        }else{
            return response()->json([
                'status' => 'failed',
                'message' =>  'unknow'
            ]);
        }
    }

    public function payMidtrans($id)
    {
        $title = 'Payment Subscription';
        $pay = true;
        DB::beginTransaction();
        try {
            $payment = Payment::select('payment.*')
                                ->join('invoice','invoice.id','=','payment.invoice_id')
                                ->where('subscription_id', $id)
                                ->whereIn('status', ['Waiting', 'Process'])
                                ->first();

            if (empty($payment)) {
                $customer = Customers::where('user_id', Auth::user()->id)->first();
                $bil = Parameters::where('id', $id)->first();

                $invoice = New Invoice;
                $invoice->date = date('Y-m-d');
                $invoice->customer_id = $customer->id;
                $invoice->total = $bil->value;
                $invoice->subscription_id = $id;
                $invoice->save();

                $payment = New Payment;
                $payment->invoice_id = $invoice->id;
                $payment->va_number = '-';
                $payment->bank = '-';
                $payment->gross_amount = $invoice->total;
                $payment->currency_id = 'Rupiah';

                $payment->save();

                $invoice->order_id = $payment->id;
                $midtrans = new CreateSnapTokenService($invoice);
                $snapToken = $midtrans->getSnapToken();

                $payment->snap_token = $snapToken;
                $payment->save();
            }else{
                $snapToken = $payment->snap_token;
            }
            DB::commit();
            $data = Customers::where('user_id', Auth::user()->id)->first();
            $list = Subscriptions::where('customer_id', $data->id)->with('subscription')->get();
            $order = Payment::select('payment.*', 'parameters.name')
                            ->join('invoice','invoice.id','=','payment.invoice_id')
                            ->join('parameters','parameters.id','=','invoice.subscription_id')
                            ->where('customer_id', $data->id)
                            ->where('payment_status', '=', 'Waiting')
                            ->get();

            $subscriptions = Parameters::where('group','=','Subscription')->where('value', '!=', 0)->orderBy('name','asc')->get();
            return view('front-end.subscriptions', compact('id','pay','snapToken', 'list', 'subscriptions', 'order'));
        } catch (\Exception $e) {
            DB::rollback();
            return back();
        }
    }
}
