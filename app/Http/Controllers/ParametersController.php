<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\DefaultCollection;
use App\Http\Requests\ParametersRequest;
use App\Models\User;
use App\Models\Parameters;
use App\Providers\PermissionsProvider;
use Session, DB, Hash, Image;

class ParametersController extends Controller
{
    public function __construct()
    {
        $this->title = "Parameters";

        $this->middleware('permission:parameters-view',['only' => 'index']);
        $this->middleware('permission:parameters-create',['only' => ['create','store']]);
        $this->middleware('permission:parameters-edit',['only' => ['edit']]);
        $this->middleware('permission:parameters-update',['only' => 'update']);
        $this->middleware('permission:parameters-delete',['only' => 'destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = $this->title;
        if($request->ajax()){
            $data = Parameters::with('createdByUser','updatedByUser')->get()->toArray();
            $kt = ktDatatable($data,$request->all());
            $request->meta = $kt;
            return response()->json(new DefaultCollection($data));
        }
        return view('parameters.index', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = 'Create '.$this->title;
        $edit = false;
        return view('parameters.create',compact('edit','title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ParametersRequest $request)
    {
        if($request->ajax()){
            DB::beginTransaction();
            try {
                $data = New Parameters;
                $data->group = $request['group'];
                $data->name = $request['name'];
                $data->value = $request['value'];

                if(isset($request['image'])){
                    $image = $this->images($request['image']);
                    $data->image_path = preg_replace('#^https?://#i', '', url('storage/parameters'));
                    $data->image_name = $image;
                };
                $data->save();
                DB::commit();
                return response()->json([
                    'status' => 'success',
                    'message' =>  ''
                ]);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json([
                    'status' => 'error',
                    'message' =>  $e->getMessage()
                ],500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $title = 'Edit '.$this->title;
        $edit = true;
        $data = Parameters::where('id',$id)->firstOrFail();
        return view('parameters.create',compact('edit','data','title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ParametersRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $url = Parameters::find($id)->update($request->all());
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'message' =>  $e->getMessage()
            ],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            DB::beginTransaction();
            try {
                Parameters::where('id',$id)->delete();
                DB::commit();

                $code = 200;
                $message = 'Delet '.$this->title.' Success!';
                $status = 'success';
                
                return response()->json([
                            'code'      => $code,
                            'message'   => $message
                        ]);

            } catch (\Exception $e) {
                DB::rollback();
                $code = 500;
                $message = 'Delet '.$this->title.' Failed!';
                return response()->json([
                            'code'      => $code,
                            'message'   => $message
                        ]);
            }
        }
    }

    public function images($val)
    {
        $filenamewithextension = $val->getClientOriginalName();
        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
        $extension = $val->getClientOriginalExtension();
  
        $filenametostore = $filename.'_'.time().'.'.$extension;

        $val->storeAs('public/parameters', $filenametostore);
        $val->storeAs('public/parameters/thumbnail', $filenametostore);

        $mediumthumbnailpath = public_path('storage/parameters/thumbnail/'.$filenametostore);
        $this->createThumbnail($mediumthumbnailpath, $filenametostore, 300, 300);
        return $filenametostore;
    }

    public function createThumbnail($path, $file, $width, $height)
    {
        $img = Image::make($path);
        
        if ($img->width() > $width) { 
            $img->resize($width, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        if ($img->height() > $height) {
            $img->resize(null, $height, function ($constraint) {
                $constraint->aspectRatio();
            }); 
        }

        $img->resizeCanvas($width, $height, 'center', false, '#ffffff');
        $img->save($path);
    }
}
