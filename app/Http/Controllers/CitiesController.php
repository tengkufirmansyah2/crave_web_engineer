<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\DefaultCollection;
use App\Http\Requests\CitiesRequest;
use App\Models\User;
use App\Models\Cities;
use App\Models\States;
use App\Models\Countries;
use App\Providers\PermissionsProvider;
use Session, DB, Hash;

class CitiesController extends Controller
{
    public function __construct()
    {
        $this->title = "Cities";

        $this->middleware('permission:cities-view',['only' => 'index']);
        $this->middleware('permission:cities-create',['only' => ['create','store']]);
        $this->middleware('permission:cities-edit',['only' => ['edit']]);
        $this->middleware('permission:cities-update',['only' => 'update']);
        $this->middleware('permission:cities-delete',['only' => 'destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = $this->title;
        if($request->ajax()){
            $data = Cities::with('createdByUser','updatedByUser','state')->get()->toArray();
            $kt = ktDatatable($data,$request->all());
            $request->meta = $kt;
            return response()->json(new DefaultCollection($data));
        }
        return view('cities.index', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = 'Create '.$this->title;
        $edit = false;
        $countries = Countries::orderBy('name','asc')
                                ->select('countries.*')
                                ->join('states','states.country_id','=','countries.id')
                                ->groupBy('Countries.id')
                                ->get();
        return view('cities.create',compact('edit','countries','title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CitiesRequest $request)
    {
        if($request->ajax()){
            DB::beginTransaction();
            try {
                Cities::create($request->all());
                DB::commit();
                return response()->json([
                    'status' => 'success',
                    'message' =>  ''
                ]);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json([
                    'status' => 'error',
                    'message' =>  $e->getMessage()
                ],500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $title = 'Edit '.$this->title;
        $edit = true;
        $data = Cities::where('id',$id)->with('state')->firstOrFail();
        $countries = Countries::get();
        return view('cities.create',compact('edit','data','countries','title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CitiesRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $url = Cities::find($id)->update($request->all());
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'message' =>  $e->getMessage()
            ],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            DB::beginTransaction();
            try {
                Cities::where('id',$id)->delete();
                DB::commit();

                $code = 200;
                $message = 'Delet '.$this->title.' Success!';
                $status = 'success';
                
                return response()->json([
                            'code'      => $code,
                            'message'   => $message
                        ]);

            } catch (\Exception $e) {
                DB::rollback();
                $code = 500;
                $message = 'Delet '.$this->title.' Failed!';
                return response()->json([
                            'code'      => $code,
                            'message'   => $message
                        ]);
            }
        }
    }

    public function getCities($id){
        $data = Cities::select('id','name')
                        ->where('state_id',$id)
                        ->get();
        return response()->json(['Data'=>$data]);
    }
}
