<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use Redirect;
use Auth;
use Session;
class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.login');
    }

    public function login(LoginRequest $request){
         if(Auth::attempt(['email' => $request->email,'password' => $request->password])){
            alert('success','Berhasil masuk kedalam aplikasi!');
            return Redirect::to(route('app.beranda'));
         }else{
            alert('danger','Kesalahan pada username atau password!');
             return redirect::back();
         }
    }
    public function signout(){
        Auth::logout();
        Session::flush();
        header("Cache-Control: no-cache, no-store, must-revalidate");
        return redirect()->route('login');
    }
}
