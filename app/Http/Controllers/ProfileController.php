<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ChangePasswordRequest;
use App\Models\Url;
use App\Models\User;
use App\Models\Customers;
use App\Models\Parameters;
use App\Models\Countries;
use App\Models\Subscriptions;
use Auth, Image;
use DB;
class ProfileController extends Controller
{
    public function __construct()
    {
        $this->title = "Profile";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($value='')
    {
        $title = $this->title;
        $data = Customers::with('gender','religion','country','state','city')->where('user_id', Auth::user()->id)->first();
        $countries = Countries::get();
        return view('profile.index', compact('title', 'data', 'countries'));
    }

    public function updateProfile(Request $request)
    {
        if($request->ajax()){
            DB::beginTransaction();
            try {
                $check = Customers::where('user_id', Auth::user()->id)->first();
                if (isset($check)) {
                    $data = Customers::find($check->id);
                }else{
                    $data = New Customers;
                    $data->user_id = Auth::user()->id;
                }
                $data->first_name = $request['first_name'];
                $data->last_name = $request['last_name'];
                $data->birth_place = $request['birth_place'];
                $data->birth_date = $request['birth_date'];
                $data->gender_id = $request['gender_id'];
                $data->religion_id = $request['religion_id'];
                $data->email = $request['email'];
                $data->mobile = $request['mobile'];
                $data->blood_type = $request['blood_type'];
                $data->address = $request['address'];
                $data->country_id = $request['country_id'];
                $data->state_id = $request['state_id'];
                $data->city_id = $request['city_id'];
                $data->zip_code = $request['zip_code'];

                if(isset($request['photo_name'])){
                    $image = $this->images($request['photo_name']);
                    $data->photo_name = $image;
                    $data->photo_type = $request['photo_name']->getClientOriginalExtension();
                    $data->photo_path = preg_replace('#^https?://#i', '', url('storage/customers'));
                };
                $data->save();

                $subscriptions = Parameters::where('group','=','Subscription')->where('name','=','Free')->first();

                if(isset($subscriptions)){
                    $invoice = New Subscriptions;
                    $invoice->subscription_id = $subscriptions->id;
                    $invoice->customer_id = $data->id;
                    $invoice->start_date = date('Y-m-d');
                    $invoice->end_date = date("Y-m-d", strtotime("+1 month"));
                    $invoice->save();
                }

                DB::commit();
                return response()->json([
                    'status' => 'success',
                    'message' =>  ''
                ]);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json([
                    'status' => 'error',
                    'message' =>  $e->getMessage()
                ],500);
            }
        }
    }

    public function indexAccountInformation()
    {
        $title = $this->title;
        return view('profile.accountInformation', compact('title'));
    }

    public function updateAccountInformation(ChangePasswordRequest $request){
        DB::beginTransaction();
        try {
            $password = Hash::make($request['password']);
            $user = User::where('id',auth()->user()->id)
                        ->update([
                            'password' => $password
                        ]);
            DB::commit();
            return response()->json([
                'status' => 'success',
                'message' =>  ''
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'message' =>  $e->getMessage()
            ],500);
        }
    }

    public function getParams($id){
        $data = Parameters::select('id','name','value')
                        ->where('group',$id)
                        ->get();
        return response()->json(['Data'=>$data]);
    }

    public function images($val)
    {
        $filenamewithextension = $val->getClientOriginalName();
        $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
        $extension = $val->getClientOriginalExtension();
  
        $filenametostore = $filename.'_'.time().'.'.$extension;

        $val->storeAs('public/customers', $filenametostore);
        $val->storeAs('public/customers/thumbnail', $filenametostore);

        $mediumthumbnailpath = public_path('storage/customers/thumbnail/'.$filenametostore);
        $this->createThumbnail($mediumthumbnailpath, $filenametostore, 300, 300);
        return $filenametostore;
    }

    public function createThumbnail($path, $file, $width, $height)
    {
        $img = Image::make($path);
        
        if ($img->width() > $width) { 
            $img->resize($width, null, function ($constraint) {
                $constraint->aspectRatio();
            });
        }

        if ($img->height() > $height) {
            $img->resize(null, $height, function ($constraint) {
                $constraint->aspectRatio();
            }); 
        }

        $img->resizeCanvas($width, $height, 'center', false, '#ffffff');
        $img->save($path);
    }
}
