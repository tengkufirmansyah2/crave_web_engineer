<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PermissionsRequest;
use App\Http\Resources\PermimssionsCollection;
use App\Providers\PermissionsProvider;
use App\Models\Permissions;
use App\Models\Url;
use DB, Session;
class PermissionsController extends Controller
{
    public function __construct()
    {
        $this->title = "Permissions";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (PermissionsProvider::has($request, 'permissions-view')){
            $title = $this->title;
            if($request->ajax()){
                $permissions = Permissions::get()->toArray();
                $kt = ktDatatable($permissions,$request->all());
                $request->meta = $kt;
                return response()->json(new PermimssionsCollection($permissions));
            }
            return view('settings.permissions.index', compact('title'));
        }else{
            Session::flash('message', 'Anda tidak memiliki akses ke '.$this->title.'!'); 
            Session::flash('alert-class', 'alert-danger'); 
            return redirect('/app');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (PermissionsProvider::has($request, 'permissions-create')){
            $title = $this->title;
            $edit = false;
            return view('settings.permissions.create',compact('edit','title'));
        }else{
            Session::flash('message', 'Anda tidak memiliki akses ke '.$this->title.'!'); 
            Session::flash('alert-class', 'alert-danger'); 
            return redirect('/app');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermissionsRequest $request)
    {
        if (PermissionsProvider::has($request, 'permissions-create')){
            if($request->ajax()){
                DB::beginTransaction();
                try {
                    Permissions::create($request->all());
                    DB::commit();
                    return response()->json([
                        'status' => 'success',
                        'message' =>  ''
                    ]);
                } catch (\Exception $e) {
                    DB::rollback();
                    return response()->json([
                        'status' => 'error',
                        'message' =>  $e->getMessage()
                    ],500);
                }
            }
        }else{
            Session::flash('message', 'Anda tidak memiliki akses ke '.$this->title.'!'); 
            Session::flash('alert-class', 'alert-danger'); 
            return redirect('/app');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if (PermissionsProvider::has($request, 'permissions-edit')){
            $title = $this->title;
            $edit = true;
            $permissions = Permissions::where('id',$id)->firstOrFail();
            return view('settings.permissions.edit',compact('edit','title','permissions'));
        }else{
            Session::flash('message', 'Anda tidak memiliki akses ke '.$this->title.'!'); 
            Session::flash('alert-class', 'alert-danger'); 
            return redirect('/app');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (PermissionsProvider::has($request, 'permissions-update')){
            DB::beginTransaction();
            try {
                $permissions = Permissions::find($id)
                                ->update($request->all());
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json([
                    'status' => 'error',
                    'message' =>  $e->getMessage()
                ],500);
            }
        }else{
            Session::flash('message', 'Anda tidak memiliki akses ke '.$this->title.'!'); 
            Session::flash('alert-class', 'alert-danger'); 
            return redirect('/app');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (PermissionsProvider::has($request, 'permissions-edit')){
            if($request->ajax()){
                DB::beginTransaction();
                try {
                    Permissions::where('id',$id)->delete();
                    DB::commit();

                    $code = 200;
                    $message = 'Delet '.$this->title.' Success!';
                    $status = 'success';
                    
                    return response()->json([
                                'code'      => $code,
                                'message'   => $message
                            ]);

                } catch (\Exception $e) {
                    DB::rollback();
                    $code = 500;
                    $message = 'Delet '.$this->title.' Failed!';
                    return response()->json([
                                'code'      => $code,
                                'message'   => $message
                            ]);
                }
            }
        }else{
            DB::rollback();
            $code = 402;
            $message = 'Delet '.$this->title.' Failed!';
            return response()->json([
                        'code'      => $code,
                        'message'   => $message
                    ]);
        }
    }

    public function generate(Request $request, $id) {
        if (PermissionsProvider::has($request, 'permissions-generate')){
            if($request->ajax()){
                DB::beginTransaction();
                try {
                    $url = Url::where('id',$id)->first();
                    DB::commit();
                    $permissions = array('create','edit','update','view','delete');

                    foreach($permissions as $val) {
                        permissions::create([
                            'name' => ucwords($url->name.' '.$val),
                            'slug' => $url->url.'-'.$val,
                        ]);
                    }

                    $code = 200;
                    $message = 'Proses '.$this->title.' Success!';
                    $status = 'success';
                    
                    return response()->json([
                                'code'      => $code,
                                'message'   => $message
                            ]);

                } catch (\Exception $e) {
                    DB::rollback();
                    $code = 500;
                    $message = 'Proses '.$this->title.' Failed!';
                    return response()->json([
                                'code'      => $code,
                                'message'   => $message
                            ]);
                }
            }
        }else{
            Session::flash('message', 'Anda tidak memiliki akses ke '.$this->title.'!'); 
            Session::flash('alert-class', 'alert-danger'); 
            return redirect('/app');
        }
    }
}
