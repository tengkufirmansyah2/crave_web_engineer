<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RoleRequest;
use App\Http\Resources\DefaultCollection;
use App\Http\Requests\UsersManagementRequest;
use App\Models\User;
use App\Models\Role;
use App\Providers\PermissionsProvider;
use Session, DB, Hash;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->title = "Users";

        $this->middleware('permission:users-view',['only' => 'index']);
        $this->middleware('permission:users-edit',['only' => ['edit', 'destroy']]);
        $this->middleware('permission:users-update',['only' => 'update']);
        $this->middleware('permission:users-create',['only' => ['create','store']]);
        $this->middleware('permission:users-config',['only' => ['permission','menu']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $title = $this->title;
        if($request->ajax()){
            $data = User::with('role')->get()->toArray();
            $kt = ktDatatable($data,$request->all());
            $request->meta = $kt;
            return response()->json(new DefaultCollection($data));
        }
        return view('settings.users.index', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $title = 'Tambah '.$this->title.' Baru';
        $edit = false;
        $role = Role::orderBy('name','asc')
                    ->get();
        return view('settings.users.create',compact('edit','role','title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        if($request->ajax()){
            DB::beginTransaction();
            try {
                $request['password'] = Hash::make($request['password']);
                $user = User::create($request->all());
                DB::commit();
                return response()->json([
                    'status' => 'success',
                    'message' =>  ''
                ]);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json([
                    'status' => 'error',
                    'message' =>  $e->getMessage()
                ],500);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        $title = 'Edit '.$this->title;
        $edit = true;
        $role = Role::orderBy('name','asc')
                    ->get();
        $data = User::where('id',$id)->firstOrFail();
        return view('settings.users.create',compact('edit','data','role','title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UsersManagementRequest $request, $id)
    {
        DB::beginTransaction();
        if($request->password && $request->password_confirm){
            $request['password'] = Hash::make($request->password);
        }
        try {
            if (isset($request['email_verified'])) {
                $request['email_verified_at'] = date('Y-m-d H:i:s');
            }
            if($request->password){
                $user = User::find($id)
                            ->update($request->all());
            }else{
                $user = User::find($id)
                            ->update($request->except('password'));
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'status' => 'error',
                'message' =>  $e->getMessage()
            ],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if($request->ajax()){
            DB::beginTransaction();
            try {
                Role::where('id',$id)->delete();
                DB::commit();

                $code = 200;
                $message = 'Delet '.$this->title.' Success!';
                $status = 'success';
                
                return response()->json([
                            'code'      => $code,
                            'message'   => $message
                        ]);

            } catch (\Exception $e) {
                DB::rollback();
                $code = 500;
                $message = 'Delet '.$this->title.' Failed!';
                return response()->json([
                            'code'      => $code,
                            'message'   => $message
                        ]);
            }
        }
    }
}
