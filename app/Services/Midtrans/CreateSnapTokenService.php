<?php
 
namespace App\Services\Midtrans;
 
use Midtrans\Snap;
 
class CreateSnapTokenService extends Midtrans
{
    protected $order;
 
    public function __construct($order)
    {
        parent::__construct();
 
        $this->order = $order;
    }
 
    public function getSnapToken()
    {
        $params = [
            'transaction_details' => [
                'order_id' => $this->order->order_id,
                'gross_amount' => $this->order->total,
            ],
            'customer_details' => [
                'first_name' => $this->order->customer->first_name,
                'email' => $this->order->customer->email,
                'phone' => $this->order->customer->mobile,
            ]
        ];
 
        $snapToken = Snap::getSnapToken($params);
 
        return $snapToken;
    }
}