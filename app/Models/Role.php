<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Traits\Uuid;

class Role extends Model
{
    use Uuid;

    protected $table = 'role';

    protected $primaryKey = 'id';
    
    protected $fillable = ['name'];

    // disabled timestamps data
    public $timestamps = true;
    public $incrementing = false;

    // disable update col id
    protected $guarded = ['id'];
}