<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
use App\Traits\RelationActionBy;

class Templates extends Model
{
    use Uuid, RelationActionBy;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'templates';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    public $incrementing = false;

    protected $appends = ['language'];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'desc',
        'hit',
        'version',
        'responsive',
        'subscription_id',
        'language_id',
        'path',
        'image',
        'file',
        'created_by',
        'updated_by'
    ];

    public function subscription()
    {   
        return $this->belongsTo(Parameters::class, $this->subscription_id)->where('group','=','Subscription');
    }

    public function getLanguageAttribute() {
        $exp = explode(',', $this->language_id);
        $data = Parameters::whereIn('id', $exp)->get();
        return $data;
    }
}
