<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Traits\Uuid;

class RolePermissions extends Model
{
    use Uuid;

    protected $table = 'role_permissions';

    protected $primaryKey = 'id';
    
    protected $fillable = ['permission_id','role_id','created_by','updated_by'];

    // disabled timestamps data
    public $timestamps = true;

    public $incrementing = false;

    // disable update col id
    protected $guarded = ['id'];

}