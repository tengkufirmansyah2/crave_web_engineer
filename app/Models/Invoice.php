<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
use App\Traits\RelationActionBy;

class Invoice extends Model
{
    use Uuid, RelationActionBy;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'invoice';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    public $incrementing = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date',
        'customer_id',
        'total',
        'subscription_id',
        'status',
        'created_by',
        'updated_by',
    ];

    public function customer()
    {   
        return $this->belongsTo(Customers::class,'customer_id');
    }

    public function subscription()
    {   
        return $this->belongsTo(Parameters::class,'subscription_id')->where('group','=','Subscription');
    }
}
