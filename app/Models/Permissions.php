<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Traits\Uuid;

class Permissions extends Model
{
    use Uuid;
 
    protected $table = 'permissions';

    protected $primaryKey = 'id';
    
    protected $fillable = ['name','slug'];

    // disabled timestamps data
    public $timestamps = true;

    public $incrementing = false;

    // disable update col id
    protected $guarded = ['id'];
}