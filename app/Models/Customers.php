<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
use App\Traits\RelationActionBy;
use DB;

class Customers extends Model
{
    use Uuid, RelationActionBy;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'customers';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    public $incrementing = false;

    protected $appends = ['subscription'];
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'first_name',
        'last_name',
        'birth_place',
        'birth_date',
        'gender_id',
        'religion_id',
        'email',
        'mobile',
        'blood_type',
        'address',
        'country_id',
        'state_id',
        'city_id',
        'zip_code',
        'photo_name',
        'photo_type',
        'photo_path',
        'created_by',
        'updated_by',
    ];


    public function gender()
    {   
        return $this->belongsTo(Parameters::class,'gender_id')->where('group','=','GENDER');
    }

    public function religion()
    {   
        return $this->belongsTo(Parameters::class,'religion_id')->where('group','=','RELIGION');
    }

    public function country()
    {   
        return $this->belongsTo(Countries::class,'country_id');
    }

    public function state()
    {   
        return $this->belongsTo(States::class,'state_id');
    }

    public function city()
    {   
        return $this->belongsTo(Cities::class,'city_id');
    }

    public function user()
    {   
        return $this->belongsTo(User::class,'user_id');
    }

    public function getSubscriptionAttribute() {
        $data = Subscriptions::select('*')
                            ->join('parameters','parameters.id','=','subscriptions.subscription_id')
                            ->where('subscriptions.customer_id', $this->id)
                            ->where('end_date','>=', date('Y-m-d'))
                            ->orderBy(DB::raw('cast(value as unsigned)'), 'desc')
                            ->first();
        return $data;
    }
}
