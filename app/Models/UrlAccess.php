<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Traits\Uuid;

class UrlAccess extends Model
{
    use Uuid;

    protected $table = 'url_access';

    protected $primaryKey = 'id';
    
    protected $fillable = ['url_id','role_id'];

    // disabled timestamps data
    public $timestamps = true;

    public $incrementing = false;

    // disable update col id
    protected $guarded = ['id'];

}