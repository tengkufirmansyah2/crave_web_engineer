<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
use App\Traits\RelationActionBy;

class Countries extends Model
{
    use Uuid, RelationActionBy;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'countries';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    public $incrementing = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','created_by','updated_by'];

    public function states()
    {   
        return $this->hasMany(States::class,'country_id');
    }
}
