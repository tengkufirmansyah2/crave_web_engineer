<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
use App\Traits\RelationActionBy;

class Activities extends Model
{
    use Uuid, RelationActionBy;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'activities';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    public $incrementing = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'reference',
        'reference_id',
        'note',
        'is_read',
        'created_by',
        'updated_by',
    ];


    public function user()
    {   
        return $this->belongsTo(User::class,'user_id');
    }

    public function createdBy()
    {   
        return $this->belongsTo(User::class,'created_by');
    }
}
