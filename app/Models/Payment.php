<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
use App\Traits\RelationActionBy;

class Payment extends Model
{
    use Uuid, RelationActionBy;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'payment';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    public $incrementing = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'invoice_id',
        'va_number',
        'bank',
        'gross_amount',
        'currency_id',
        'payment_status',
        'snap_token',
        'message',
        'created_by',
        'updated_by'
    ];

    public function invoice()
    {   
        return $this->belongsTo(Invoice::class,'invoice_id');
    }
}
