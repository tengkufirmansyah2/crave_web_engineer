<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;
use App\Traits\RelationActionBy;

class Parameters extends Model
{
    use Uuid, RelationActionBy;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'parameters';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    public $incrementing = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'group',
        'name',
        'value',
        'image_path',
        'image_name',
        'created_by',
        'updated_by'
    ];

}
