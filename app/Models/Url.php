<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuid;

class Url extends Model
{
    use Uuid;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'url';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    
    public $incrementing = false;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['parent_id','url','name','icon','order','position'];

    public function children()
    {   
        return $this->hasMany(self::class,'parent_id')->orderBy('order','asc')
                ->select('url.id','url.parent_id','url.url','url.icon','url.position','url.name')
                ->join('url_access','url_access.url_id','url.id')
                ->join('users','users.role_id','url_access.role_id')
                ->where('users.id', Auth::user()->id);
    }

    public function parent()
    {   
        return $this->belongsTo(self::class,'parent_id');
    }
}
