<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Auth\EmailVerificationRequest;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\AppSettingsController;
use App\Http\Controllers\UrlController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\PermissionsController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\BerandaController;
use App\Http\Controllers\CountriesController;
use App\Http\Controllers\StatesController;
use App\Http\Controllers\CitiesController;
use App\Http\Controllers\ParametersController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\CustomersController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\OrdersController;

use App\Http\Controllers\TemplatesController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[HomeController::class,'index']);
Route::get('/template/{id}',[HomeController::class,'detail']);
Route::get('/template/proses/{id}',[HomeController::class,'download']);
Route::get('/subscriptions',[HomeController::class,'listSubcriptions']);
Route::get('/subscriptions/check/{id}',[HomeController::class,'checkSubcriptions']);
Route::get('/subscriptions/payment/{id}',[HomeController::class,'payMidtrans']);

Route::get('/home', function () {
    return redirect('/');
});

Auth::routes();

Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware('auth')->name('verification.notice');

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();
 
    return redirect('/app/beranda');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();
 
    return back()->with('message', 'Verification link sent!');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');

Route::get('/forgot-password', function () {
    return view('auth.forgot-password');
})->middleware('guest')->name('forgot-password'); //--password.request

Route::group(['middleware' => ['auth', 'verified']], function () {
    Route::group(['prefix' => 'profile','as' => 'profile.'],function(){
        Route::get('/get-params/{id}',[ProfileController::class,'getParams']);

        Route::get('/',[ProfileController::class,'index'])->name('index');
        Route::post('/',[ProfileController::class,'updateProfile'])->name('update');
        Route::get('/account-information',[ProfileController::class,'indexAccountInformation'])->name('account-information');
        Route::post('/account-information',[ProfileController::class,'updateAccountInformation'])->name('account-information.update');
    });
    Route::group(['prefix' => 'app','as' => 'app.'],function(){
        

        Route::get('/signout',[AuthController::class,'signout'])->name('auth.logout');
            
        Route::get('/',[BerandaController::class,'index']);
        Route::get('/beranda',[BerandaController::class,'index'])->name('beranda');

        Route::match(['GET','POST'],'app-settings', [AppSettingsController::class,'index'])->name('app_setting');

        Route::resource('/url',UrlController::class);

        Route::resource('/permissions',PermissionsController::class);
        Route::get('permissions/generate/{id}', [PermissionsController::class,'generate']);

        Route::resource('/role',RoleController::class);

        Route::match(array('GET','POST'),'/role/configuration-permissions/{id}', [RoleController::class,'permission']);
        Route::match(array('GET','POST'),'/role/configuration-menus/{id}', [RoleController::class,'menu']);
        
        Route::resource('/users',UsersController::class);
        
        //----- Module Master -----//
        Route::resource('/countries',CountriesController::class);
        Route::resource('/states',StatesController::class);
        Route::get('get-states/{id}', [StatesController::class,'getStates']);

        Route::resource('/cities',CitiesController::class);
        Route::get('get-cities/{id}', [CitiesController::class,'getCities']);

        Route::resource('/parameters',ParametersController::class);

        Route::resource('/templates',TemplatesController::class);

        Route::resource('/customers',CustomersController::class);

        Route::resource('/orders',OrdersController::class);

        Route::resource('/invoice',InvoiceController::class);
        Route::group(['prefix' => 'invoice','as' => 'invoice.'],function(){
            Route::get('/pdf/{id}', [InvoiceController::class,'InvoicePDF'])->name('pdf');
            Route::post('/payment-update/{id}', [InvoiceController::class,'updatePayment'])->name('payment.update');
        });
        
        Route::get('/get-params/{id}',[BerandaController::class,'getParams']);
    });
});